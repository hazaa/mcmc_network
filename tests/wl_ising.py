# -*- coding: utf-8 -*- 

"""
Comment on results: 
* problems wit RNG solved.

* problems with binning discrete energy levels.
* the full range of theoretically reachable energy levels is not covered.

"""


import sys
sys.path.append('../');

import numpy as np
import wl_mcmc_network as wlmcmc  
import matplotlib.pyplot as plt
import dataset
import utils
import warnings


class TestClass_wl_ising():
	def setUp(self):
		pass
	def tearDown(self):
		pass
	def test_seed(self):
		"""
		visually check the succession of n_old,m_old
		"""
		N,M = 20,20
		field = np.random.choice([-1, 1], size=(N, M))
		isi =wlmcmc.Ising(field,N,M)	
		isi._set_seed(10)
		for i in range(3):
			move_done = isi.propose_move(0)
			isi._print_state()
		isi._set_seed(10)
		for i in range(3):
			move_done = isi.propose_move(0)
			isi._print_state()	
	def test_rollback(self):
		"""
		visually check that energy goes back to initial state after rollback
		"""
		N,M = 20,20
		field = np.random.choice([-1, 1], size=(N, M))
		isi =wlmcmc.Ising(field,N,M) 
		isi._print_state()
		isi._compute_energy() ; print "compute energy"
		isi._print_state()
		dummy=0 # for compatibility
		move_done = isi.propose_move(dummy) ; print "propose move"
		isi._print_state()
		isi.update_energy(move_done) ; print "update_energy"
		isi._print_state()
		if(True): isi.rollback(dummy); print "rollback"
		isi._print_state()
    
	def test_wl_simulate(self):  
		"""
		DEPRECATED : should use wlmcmc.wl
		
		"""	  
		warnings.warn('deprecated: see wlmcmc.wl')
		
		bins=50
		#nsteps=1000
		fmin=0.001
		finit=1.0 
		rt = 10 # 	nround_trips

		N,M = 20,20
		lowerBound = -N*M
		upperBound = N*M
		
		lnh = wlmcmc.Histogram(lowerBound, upperBound, bins)
		#nsamp=30
		field = np.random.choice([-1, 1], size=(N, M))
		isi =wlmcmc.Ising(field,N,M) 
		isi._compute_energy() 
		lnh = wlmcmc.wl_simulate_net(isi,lnh,finit=finit, fmin=fmin,round_trips=rt,
									do_copy=False, verbose=1)
		# ---------------							
		# plot
		plot_lnh(lnh)							

	def test_wl_simulate_copy(self): 
		"""
		DEPRECATED : should use wlmcmc.wl
		
		check copy
		"""	
		warnings.warn('deprecated: see wlmcmc.wl')
		
		# param
		count_max = 1000
		ener = np.zeros(count_max,dtype=float)
		count = np.zeros(1,dtype=int)
		f = utils.get_copy_func_ising(ener,count,count_max )
		# 
		bins=50
		#nsteps=1000
		fmin=0.01
		finit=1.0 
		rt = 10 # 	nround_trips
		N,M = 20,20
		lowerBound = -N*M
		upperBound = N*M
		lnh = wlmcmc.Histogram(lowerBound, upperBound, bins)
		field = np.random.choice([-1, 1], size=(N, M))
		isi =wlmcmc.Ising(field,N,M) 
		isi._compute_energy() 
		lnh = wlmcmc.wl_simulate_net(isi,lnh,finit=finit, fmin=fmin,round_trips=rt,
									do_copy=True, copy_lb=lowerBound, copy_ub=upperBound,copy_func=f,verbose=1	)
		# ---------------							
		# plot
		plot_lnh(lnh)
		plt.hist(ener,50)

	def test_wl2_copy(self): 
		"""
		check wlmcmc.wl and copy mechanism
		"""	
		# param
		count_max = 10000
		ener = np.zeros(count_max,dtype=float)
		count = np.zeros(1,dtype=int)
		f = utils.get_copy_func_ising(ener,count,count_max )
		# 
		bins=30
		finit=1.0 ; fmin= 0.0001
		N,M = 20,20
		lb = -N*M # should be -2*N*M
		ub = N*M
		lnh = wlmcmc.Histogram(lb,ub, bins)
		field = np.random.choice([-1, 1], size=(N, M))
		isi =wlmcmc.Ising(field,N,M) 
		isi._compute_energy() 
		lnh,f = wlmcmc.wl(isi, lnh, finit=finit,fmin = fmin,niter=30000000 ,do_copy=1,
			   copy_lb=lb, copy_ub=ub,copy_func=f,
			   flatness=0.80, verbose=2)
		print 'f=',f	   
		# ---------------							
		# plot
		#%matplotlib
		plot_lnh(lnh)
		plt.hist(ener[0:count[0]],50)
	  
	def test_wl2_step_by_step_from_low_energy(self):
		"""
		start from a low energy state
		"""
		count_max = 10000
		ener = np.zeros(count_max,dtype=float)
		count = np.zeros(1,dtype=int)
		f = utils.get_copy_func_ising(ener,count,count_max )
		# 
		bins=50
		finit=1.0 
		N,M = 20,20
		lb = -2*N*M
		ub = 2*N*M
		lnh = wlmcmc.Histogram(lb,ub, bins)
		field = np.ones((N, M),dtype=int)
		isi =wlmcmc.Ising(field,N,M) 
		isi._compute_energy() 
		isi._print_state()
		#
		move_done = isi.propose_move(0)
		isi._print_state()
		isi.update_energy(move_done)
		isi._print_state()
	def test_wl2_step_by_step_from_high_energy(self):
		"""
		start from a high energy state
		"""
		count_max = 10000
		ener = np.zeros(count_max,dtype=float)
		count = np.zeros(1,dtype=int)
		f = utils.get_copy_func_ising(ener,count,count_max )
		# 
		bins=50
		finit=1.0 
		N,M = 20,20
		lb = -2*N*M
		ub = 2*N*M
		lnh = wlmcmc.Histogram(lb,ub, bins)
		field = dataset.get_high_energy_ising(N,M)
		isi =wlmcmc.Ising(field,N,M) 
		isi._compute_energy() 
		isi._print_state()
		#
		move_done = isi.propose_move(0)
		isi._print_state()
		isi.update_energy(move_done)
		isi._print_state()	

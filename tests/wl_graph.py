# -*- coding: utf-8 -*- 

import sys
sys.path.append('../');

import numpy as np
import wl_mcmc_network as wlmcmc  
import scipy.sparse

import dataset
import utils

class TestClass_GraphCSR_WL():
	"""
	test WL sampling with GraphCSR objects
		
	"""
	def setUp(self):
		pass
	def tearDown(self):
		pass
	def test_wl_GraphCSR_base(self):
		"""
		dataset = base 
		"""	
		gCSR0,lnh,p =  dataset.get_gCSR_lnh(data='base')
		bins=p['hist']['bins']
		finit=p['sampler']['finit']
		fmin =p['sampler']['fmin'] 
		lb= p['hist']['lb']
		ub = p['hist']['ub']
		def func(dum1,dum2):
			pass
		lnh,f = wlmcmc.wl(gCSR0,lnh,finit=finit,fmin = fmin,niter=50000 ,
					do_copy=0,
				   copy_lb=lb, copy_ub=ub,copy_func=func,
				   flatness=0.90, verbose=2	)
		print 'f=',f	   
		plot_lnh(lnh)
	def test_wl_GraphCSR_x0_nls(self):
		"""
		dataset = x0_nls
		"""	
		gCSR0,lnh,p =  dataset.get_gCSR_lnh(data='x0_nls')
		bins=p['hist']['bins']
		finit=p['sampler']['finit']
		fmin =p['sampler']['fmin'] 
		lb= p['hist']['lb']
		ub = p['hist']['ub']
		def func(dum1,dum2):
			pass
		lnh,f = wlmcmc.wl(gCSR0,lnh,finit=finit,fmin = fmin,niter=1000000 ,
					do_copy=0,
				   copy_lb=lb, copy_ub=ub,copy_func=func,
				   flatness=0.90, verbose=2	)
		print 'f=',f	   
		plot_lnh(lnh)
	def test_wl_GraphCSR_x0_nls_L1(self):
		"""
		dataset = x0_nls_L1
		"""	
		gCSR0,lnh,p = dataset.get_gCSR_lnh(data='x0_nls_L1')
		bins=p['hist']['bins']
		finit=p['sampler']['finit']
		fmin =p['sampler']['fmin'] 
		lb= p['hist']['lb']
		ub = p['hist']['ub']
		def func(dum1,dum2):
			pass
		lnh,f = wlmcmc.wl(gCSR0,lnh,finit=finit,fmin = fmin,niter=1000000 ,
					do_copy=0,
				   copy_lb=lb, copy_ub=ub,copy_func=func,
				   flatness=0.90, verbose=2	)
		print 'f=',f	   
		plot_lnh(lnh)
	def test_wl_GraphCSR_x0_nls_copy(self):	
		"""
		dataset = x0_nls_L1 + copy
		"""				
	def test_wl_GraphCSR_x0_nls_x0_FiCM_weight_only(self):
		"""
		change weight_only
		"""
		gCSR0,lnh,p = dataset.get_gCSR_lnh(data='x0_FiCM')
		bins=p['hist']['bins']
		finit=p['sampler']['finit']
		fmin =p['sampler']['fmin'] 
		lb= -80#p['hist']['lb']
		ub = 80#p['hist']['ub']
		move_type = 2 # weight only
		def func(dum1,dum2):
			pass
		lnh,f = wlmcmc.wl(gCSR0,lnh,move_type=move_type,finit=finit,fmin = fmin,niter=1000000 ,
					do_copy=0,
				   copy_lb=lb, copy_ub=ub,copy_func=func,
				   flatness=0.90, verbose=2	)
		print 'f=',f	   
		plot_lnh(lnh)		
		
class TestClass_GraphCSR_numpy():
	"""
	test basic operations on GraphCSR objects 
	
	compute the dot products necessary for energy 
	using numpy
	
	no Wang-Landau sampling in this class
	"""
	def setUp(self):
		pass
	def tearDown(self):
		pass
	def test_rollback(self):
		"""
		visually check that delta and energy goes back to initial state after rollback
		"""
		sys,lnh,p = dataset.get_gCSR_lnh(data='x0_nls_L1')
		#sys._print_state()
		sys.get_energy() ; print "compute energy"
		sys._print_state()
		move_done = 0
		while move_done==0: move_done=sys.propose_move(0)
		sys._print_state()
		sys.update_energy(move_done) ; print "update_energy"
		sys._print_state()
		if(True): sys.rollback(move_done); print "rollback"
		sys._print_state()
		

class TestClass_GraphCSR_dot_nonumpy():	
	"""
	DEPRECATED
	
	no Wang-Landau sampling in this class
	
	compute the dot products necessary for energy 
	without using numpy
	
	To run all tests of the class:
	$nosetests wl_mcmc.py:TestClass_wl_mcmc
	$nosetests wl_mcmc.py:TestClass_wl_mcmc.test_get_energy
	"""
	def setUp(self):
		nf=20
		nr,nc = 50,nf**2
		A_csr = scipy.sparse.rand(nr,nc,0.1,format='csr')
		A_csc = scipy.sparse.csc_matrix(A_csr)
		#energy_arg_A = scipy.sparse.identity(10000, dtype='int8', format='csc')
		b = scipy.sparse.csr_matrix(np.ones(nr))
		x0_csr = scipy.sparse.rand(nf,nf,0.1,format='csr')
		#g0 = wlmcmc.Graph(A,x0_csr,b,'residual') # sparse matrix
		self.gCSR0 = wlmcmc.GraphCSR(A_csr.data, A_csr.indices, A_csr.indptr,
								A_csc.data, A_csc.indices, A_csc.indptr,
								nr,nc,
								x0_csr.data, x0_csr.indices,x0_csr.indptr,
								nf,nf,						
								b.toarray().flatten()) 
		self.gCSR0.init()						    
		bins=30
		self.upperBound = gCSR0._get_delta_norm()
		self.lowerBound = upperBound-15.
		print 'upperBound=',self.upperBound
		lnh = wlmcmc.Histogram(self.lowerBound, self.upperBound, bins)
	def tearDown(self):
		pass
	def test_csr2row(self):
		A = scipy.sparse.csr_matrix([[1,0,2],[3,0,4]])	
		assert wlmcmc.csr2row(0, A.indptr,A.nnz)==0 # the term "1" is on row 0
		assert wlmcmc.csr2row(1, A.indptr,A.nnz)==0 # the term "2" is on row 0
		assert wlmcmc.csr2row(2, A.indptr,A.nnz)==1 # the term "3" is on row 0
		assert wlmcmc.csr2row(3, A.indptr,A.nnz)==1 # the term "4" is on row 1
		
	def test_get_energy(self):
		nf=10
		nr,nc = 1000,nf**2
		A = scipy.sparse.rand(nr,nc,0.1,format='csr')
		b = scipy.sparse.csr_matrix(np.ones(nr))
		x0_csr = scipy.sparse.rand(nf,nf,0.1,format='csr')
		g0 = wlmcmc.Graph(A,x0_csr,b,'residual') # sparse matrix
		gCSR0 = wlmcmc.GraphCSR(A.data, A.indices, A.indptr,
								nr,nc,
								x0_csr.data, x0_csr.indices,x0_csr.indptr,
								nf,nf,						
								b.toarray().flatten(),
								'residual')     
		lowerBound, bins=0.,20
		upperBound = gCSR0.get_energy()
		#
		x0_csr_flat = x0_csr.toarray().reshape((nf**2,1))
		Ax = A.dot(x0_csr_flat)
		e = np.sum((Ax-b.toarray().flatten())**2)
		print upperBound,e
		assert np.allclose(upperBound,e) 

	def test_add_dot_A_xei_CSC(self):
		"""
		test add_dot_A_xei_CSC returns a column
		"""
		niter = 100
		nf=10
		nr,nc = 1000,nf**2
		delta_x_j=1.

		for it in range(niter):
			j = np.random.randint(low=0,high=nc)
			A_csc = scipy.sparse.rand(nr,nc,0.1,format='csc')
			result = np.zeros(nr,dtype = np.float)          
			wlmcmc.add_dot_A_xei_CSC(A_csc.data, A_csc.indices, A_csc.indptr, 
								result, j,  delta_x_j) 			
			assert np.allclose(result,	A_csc[:,j].toarray().flatten())
						
	def test__get_x0():
		"""
		test _get_x0
		"""
		self.gCSR0._get_x0()
	def test_add_dot_A_xei_CSC_vs_numpy(self):
		"""
		test dot_A_deltax_CSC result against numpy dot product
		"""
		niter = 100
		nf=10
		nr,nc = 1000,nf**2

		delta_x_i=1.

		for it in range(niter):
			A_csc = scipy.sparse.rand(nr,nc,0.1,format='csc')
			x0 = np.zeros(nc,dtype = np.float)
			i,j = np.random.randint(low=0,high=nc,size=2)
			if i != j:
				result = np.zeros(nr,dtype = np.float)          
				wlmcmc.add_dot_A_xei_CSC(A_csc.data, A_csc.indices, A_csc.indptr,
									result, i,delta_x_i)	  
				x0[i] = delta_x_i				
				assert np.allclose( A_csc.dot(x0),result )	
	def test_add_dot_A_xei_CSC_vs_numpy_2(self):
		"""
		test dot_A_deltax_CSC result against numpy dot product
		"""
		niter = 100
		nf=10
		nr,nc = 1000,nf**2
		nr,nc = 10,20
		delta_x_i=1.
		delta_x_j=-1.

		for it in range(niter):
			A_csc = scipy.sparse.rand(nr,nc,0.1,format='csc')
			x0 = np.zeros(nc,dtype = np.float)
			i,j = np.random.randint(low=0,high=nc,size=2)
			if i != j:
				result = np.zeros(nr,dtype = np.float)          
				wlmcmc.add_dot_A_xei_CSC(A_csc.data, A_csc.indices, A_csc.indptr,
									result, i,delta_x_i)
				wlmcmc.add_dot_A_xei_CSC(A_csc.data, A_csc.indices, A_csc.indptr,
									result,j,delta_x_j)		  
				x0[i] = delta_x_i
				x0[j] = delta_x_j	
				print "it=",it				
				assert np.allclose( A_csc.dot(x0),result)					  

""" TIMEIT

niter = 1000
nf=10
nr,nc = 1000,nf**2

delta_x_i=1.
delta_x_j=-1.
x0 = np.zeros(nc,dtype = np.float)
A = scipy.sparse.rand(nr,nc,0.1,format='csc')
def f1():
	i,j = np.random.randint(low=0,high=nc,size=2);
	x0[i] = delta_x_i
	x0[j] = delta_x_j
	result = np.zeros(nr,dtype = np.float)
	A.dot(x0)

def f2():
	i,j = np.random.randint(low=0,high=nc,size=2);
	x0[i] = delta_x_i
	x0[j] = delta_x_j
	result = np.zeros(nr,dtype = np.float)
	wlmcmc.add_dot_A_xei_CSC(A.data, A.indices, A.indptr, result,
						  i,delta_x_i)				  
	wlmcmc.add_dot_A_xei_CSC(A.data, A.indices, A.indptr, result,
						  j,delta_x_j)						  

%timeit f1()
%timeit f2()
"""

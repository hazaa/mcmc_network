# -*- coding: utf-8 -*-
#python setup.py build_ext --inplace
from distutils.core import setup, Extension
from Cython.Build import cythonize
from Cython.Compiler import Options

# Annotate: http://docs.cython.org/en/latest/src/userguide/source_files_and_compilation.html#compiler-options
# doesn't work!!
#Options.annotate = True

# Profile
# http://nbviewer.jupyter.org/gist/tillahoffmann/296501acea231cbdf5e7
# https://github.com/cython/cython/blob/master/tests/run/line_profile_test.srctree
# https://stackoverflow.com/questions/28301931/how-to-profile-cython-functions-line-by-line


setup(ext_modules = cythonize(Extension(
           "wl_mcmc_network",                                # the extension name
           sources=["wl_mcmc_network.pyx"], # the Cython source and
                                                  # additional C++ source files
           language="c++",                        # generate and compile C++ code
      ),annotate=False)
      )



# -*- coding: utf-8 -*-
"""
Created on oct 24 2018

Module:
    mcmc - mcmc
							
    
Author:
    Aurélien Hazan, after 
    
Description:
    Implementation of ...
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays,. 
    Import the module and initialize the ... ::
        >>> import 
        >>> cs = ...
    To create ::
        >>> cs.do_smthg()

References:
.. [Saracco2015] `F. Saracco, R. Di Clemente, A. Gabrielli, T. Squartini,
    Randomizing bipartite networks: the case of the World Trade Web,
    Scientific Reports 5, 10595 (2015)
    <http://www.nature.com/articles/srep10595>`_   
    
wl+mc:    
https://github.com/wmjac/pygtsa/blob/master/cgraph.pyx
https://github.com/SamplingConstrainedNetworks/code/blob/master/source/sampler.h

sparse:
https://docs.scipy.org/doc/scipy/reference/sparse.html
https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg

cython:
http://docs.cython.org/en/latest/src/tutorial/cdef_classes.html

examples:
https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/utils/graph_shortest_path.pyx
https://gist.github.com/rmcgibbo/6019670
lil https://stackoverflow.com/questions/25430866/scipy-sparse-matrices-and-cython#25453519
https://stackoverflow.com/questions/25295159/how-to-properly-pass-a-scipy-sparse-csr-matrix-to-a-cython-function

save sparse:
scipy.sparse.save_npz https://stackoverflow.com/questions/8955448/save-load-scipy-sparse-csr-matrix-in-portable-data-format
"""   

import numpy as np  
import wl_mcmc_network as wlmcmc  
import scipy.sparse

import matplotlib.pyplot as plt
import dataset
import utils

import nls_solvers

def plot_lnh(lnh):
	upperBound = lnh.get_upperBound()
	lowerBound = lnh.get_lowerBound()
	bins = lnh.bins()+1
	H = np.zeros(bins,dtype=np.int32)
	lnh.get_histogram(H)

	S = np.zeros(bins)
	lnh.get_entropy(S)

	plt.subplot(2,1,1)
	e = np.linspace(lowerBound,upperBound,bins)
	plt.plot(e,H)
	plt.ylabel('H')
	plt.subplot(2,1,2)
	plt.plot(e,S)
	plt.xlabel('energy')
	plt.ylabel('S')
	plt.show()

def test_TestCopyFunc():
	"""
	test the copying algorithm
	"""
	# init
	nf = 5
	nr_x0,nc_x0 = nf,nf
	x0_csr = scipy.sparse.rand(nr_x0,nc_x0,0.1,format='csr')
	nsamp=3
	samples_csr = scipy.sparse.csr_matrix((nsamp*nr_x0,nc_x0),dtype=float)
	# declare func
	def copy_func(x,nr,nc ):
		def myfunc(y,i):
			x[i*nr:(i+1)*nr, :] = y
		return myfunc
	f= copy_func(samples_csr , nr_x0,nc_x0)
	# test f
	f(x0_csr,0)
	i=0;assert np.allclose( np.array(x0_csr.todense()),np.array(samples_csr.todense())[i*nr_x0:(i+1)*nr_x0])
	i=1;assert np.allclose( np.zeros((nr_x0,nc_x0)),np.array(samples_csr.todense())[i*nr_x0:(i+1)*nr_x0])

	# test TestSaveFunc
	t= wlmcmc.TestCopyFunc(x0_csr.data, x0_csr.indices,x0_csr.indptr,
						nr_x0,nc_x0)
	i = 1
	t.test_copy_func(i,f)	
	assert np.allclose( np.array(x0_csr.todense()),np.array(samples_csr.todense())[i*nr_x0:(i+1)*nr_x0])			
	# check
	samples_csr.todense()						


def test_get_copy_func():
	"""
	test utils.get_copy_func 
	(NB: gCSR is not used)
	"""
	gCSR0,lnh,p = dataset.get_gCSR_lnh(data='x0_nls')
	#gCSR0,lnh,p = get_gCSR_lnh(data='x0_nls_L1')
	#----------------
	# WL + copy states
	# init
	bins=p['hist']['bins']
	nsteps=p['sampler']['nsteps']
	finit=p['sampler']['finit']
	fmin =p['sampler']['fmin'] 
	rt = p['sampler']['nround_trips']	
	nr_x0=p['system']['nr_x0']
	nc_x0=p['system']['nc_x0']

	nsamp=30
	samples_csr = scipy.sparse.csr_matrix((nsamp*nr_x0,nc_x0),dtype=float)
	ener = 1000*np.ones(nsamp)
	count = np.zeros(1,dtype=int)
	# copy func
	#f= utils.get_copy_func(samples_csr ,ener, nr_x0,nc_x0)
	f= utils.get_copy_func(samples_csr ,ener, nr_x0,nc_x0,count,nsamp)
	
	# test copy
	x0_csr = scipy.sparse.rand(nr_x0,nc_x0,0.1,format='csr')
	#i=0;f(x0_csr,1.0,i)	
	for i in range(nsamp+10): f(x0_csr,1.0)
	i=0;assert np.allclose( np.array(x0_csr.todense()),np.array(samples_csr.todense())[i*nr_x0:(i+1)*nr_x0])
################
def test_TestStdCpp():
	"""
	"""
	t = wlmcmc.TestStdCpp()
	t._print_state()
	t.test_find()
	t.test_index()

def test_AB_SFC_macro1_cpp():
	"""
	"""
	A = scipy.sparse.csr_matrix([[1,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
	ab._print_state()
def test_AB_SFC_macro1_cpp_balance():
	"""
	"""
	A = scipy.sparse.csr_matrix([[2.,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
	ab._balance_hh()
	ab._balance_firm()
	ab._print_state()
	# expected balance_hh  [-1,0,-1]
	# expected balance_firm  [2,0]
	
	
def test_AB_SFC_macro1_cpp_swap_cons_hh_degree():
	"""
	check _swap_cons_hh_degree
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	ab._print_state()
	move_done=0
	while move_done!=1:	move_done = ab._swap_cons_hh_degree()		
	ab._print_state()
	# copy state
	f= get_func_AB_SFC_macro1(A_,C_)
	ab.copy_state(f)
	# check that C doesn't change
	assert np.all((C.tolil()==C_).toarray())
	# check that A is just a permutation
	assert utils.check_permutation(A,A_)

def test_AB_SFC_macro1_cpp_swap_wage_firm_degree():
	"""
	check _swap_cons_hh_degree
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	ab._print_state()
	move_done = 0
	while move_done!=1: move_done = ab._swap_wage_firm_degree()		
	ab._print_state()
	# copy state
	f= get_func_AB_SFC_macro1(A_,C_)
	ab.copy_state(f)
	# check that A doesn't change
	assert np.all((A.tolil()==A_).toarray())
	# check that C is just a permutation
	assert utils.check_permutation(C,C_)	

def test_AB_SFC_macro1_cpp_energy():
	nf = 20
	nh = 100
	density = 0.05
	print "Generate dataset"
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced',nf=nf,nh=nh,density=density)
	nf = A.shape[0]
	nh = A.shape[1]
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	f= get_func_AB_SFC_macro1(A_,C_)
	sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)	
	n=10000
	e = np.zeros(n)
	move = np.zeros(n)
	for i in range(n):
		move[i]=sys.propose_move(move_type)
		e[i]=sys.get_energy()
	plt.subplot(2,1,1)
	plt.plot(range(n), e)
	plt.ylabel('energy')
	plt.subplot(2,1,2)
	plt.plot(range(n), move)
	plt.ylabel('move')
	plt.show()
	
	
def test_AB_SFC_macro1_cpp_swap_wage_firm_degree_rollback(self):
	"""
	check that delta and energy goes back to initial state after rollback
	"""
	for d in ['base','balanced']:
		A,C = dataset.get_AB_SFC_macro1(dataset=d)
		nf = A.shape[0]
		nh = A.shape[1]
		A_=scipy.sparse.lil_matrix((nf,nh))
		C_=scipy.sparse.lil_matrix((nf,nh))
		f= get_func_AB_SFC_macro1(A_,C_)
		sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)	
		# propose move
		move_done=0
		move_type = 2
		while move_done!=move_type: move_done=sys.propose_move(move_type)
		# compare state: must have changed
		sys.copy_state(f)
		assert not np.all((C.tolil()==C_).toarray())
		sys.rollback(move_done)
		sys.copy_state(f)
		# compare state: must have returned
		assert np.all((C.tolil()==C_).toarray())	
	
def test_AB_SFC_macro1_cpp_swap_cons_hh_degree_rollback(self):
	"""
	check that delta and energy goes back to initial state after rollback
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	f= get_func_AB_SFC_macro1(A_,C_)
	sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)	
	# propose move
	move_done=0
	move_type = 1
	while move_done!=move_type: move_done=sys.propose_move(move_type)
	# compare state: must have changed
	sys.copy_state(f)
	assert not np.all((A.tolil()==A_).toarray())
	sys.rollback(move_done)
	sys.copy_state(f)
	# compare state: must have returned
	assert np.all((A.tolil()==A_).toarray())
def test_AB_SFC_macro1_cpp_WL():
	nf = 60
	nh = 200
	density = 0.05
	niter=10000000
	print "Generate dataset"
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced_sparse',nf=nf,nh=nh,density=density)

	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	f= utils.get_func_AB_SFC_macro1(A_,C_)
	
	sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
	move_type = 0
	do_copy = 0 # !!!!!!!!!!!
	bins=50
	finit=1.0 ; fmin= 0.001
	lb,ub=3,50 # !!!!!!!!!!!
	lnh = wlmcmc.Histogram(lb,ub, bins)
	print "Start sampling:"
	lnh,f_end = wlmcmc.wl(sys, lnh, finit=finit,fmin = fmin,niter=niter,do_copy=do_copy,
	   copy_lb=lb, copy_ub=ub,copy_func=f,
	   flatness=0.80, verbose=2, move_type=move_type)
	#%matplotlib
	plot_lnh(lnh)	

def test_debt_scaling():
	"""
	plot debts when nf is increased
	"""
	nf_ = np.array([20,50,100,200,500])
	debt_ratio= np.zeros(nf_.size)
	niter = 100000
	
	bins=50
	finit=1.0 ; fmin= 0.001
	lb_pct,ub_pct=0.05,0.2 # !!!!!!!!!!!
	move_type = 0
	do_copy = 0 # !!!!!!!!!!!

	
	for i,nf in enumerate(nf_):
		nh = 10*nf
		density = 0.05
		print "Generate dataset"
		A,C = dataset.get_AB_SFC_macro1(dataset='balanced_sparse',nf=nf,nh=nh,density=density)
		lb = lb_pct * (2*A.sum())
		ub = ub_pct * (2*A.sum())

		A_=scipy.sparse.lil_matrix((nf,nh))
		C_=scipy.sparse.lil_matrix((nf,nh))
		f= utils.get_func_AB_SFC_macro1(A_,C_)
	
		sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)

		lnh = wlmcmc.Histogram(lb,ub, bins)
		print "Start sampling:"
		lnh,f_end = wlmcmc.wl(sys, lnh, finit=finit,fmin = fmin,niter=niter,do_copy=do_copy,
			copy_lb=lb, copy_ub=ub,copy_func=f,
			flatness=0.80, verbose=2, move_type=move_type)
		e = sys.get_energy()	
		debt_ratio[i] = e / (2*A.sum() )
		#debt_ratio_firm
		#debt_ratio_hh
	plt.plot(nf_,debt_ratio*100)
	plt.xlabel('nf') ; plt.xlabel('debt_ratio')
	plt.show()
	
def test():
	# degree sequence in A,C
	# strength sequence in A,C
	# local balance firm ; hh
	pass
	
def test_AB_SFC_macro1_cpp_copy_state_trajectory():
	"""
	copy successive states of A,C	
	no WL
	"""
	# initial state
	nf = 20
	nh = 100
	density = 0.05
	print "Generate dataset"
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced',nf=nf,nh=nh,density=density)
	# prepare sample
	nsamp=10
	# run simu
	sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)	
	n=10000
	for move_type in [0,1,2]:
		# prepare matrices to record samples
		A_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)
		C_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)
		ener_sample = np.zeros(nsamp)
		count = np.zeros(1,dtype=int)
		count_max=nsamp
		f = utils.get_copy_func_AB_SFC_macro1(A_sample,C_sample,ener_sample,nf,nh,count,count_max )
		# simulation
		e = np.zeros(n)
		move = np.zeros(n)
		for i in range(n):
			move[i]=sys.propose_move(move_type)
			e[i]=sys.get_energy()
			if i%int(float(n)/float(nsamp)) ==0: sys.copy_state(f)
	#plot
	plt.plot(ener_sample)		
	plt.matshow(A_sample.toarray())
	

		
def test_AB_SFC_macro1_check_move_type():
	"""
	plot move type as a function of iteration
	no WL
	"""
	# initial state
	nf = 20
	nh = 100
	density = 0.05
	print "Generate dataset"
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced',nf=nf,nh=nh,density=density)
	# prepare sample
	nsamp=10
	n=1000
	nr = 3 ; nc=2
	for i,move_type in enumerate([0,1,2]):
		# run simu
		sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)	
		# prepare matrices to record samples
		A_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)
		C_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)
		ener_sample = np.zeros(nsamp)
		count = np.zeros(1,dtype=int)
		count_max=nsamp
		f = utils.get_copy_func_AB_SFC_macro1(A_sample,C_sample,ener_sample,nf,nh,count,count_max )
		# simulation
		e = np.zeros(n)
		move = np.zeros(n)
		for j in range(n):
			move[j]=sys.propose_move(move_type)
			e[j]=sys.get_energy()
			if j%int(float(n)/float(nsamp)) ==0: sys.copy_state(f)
		#plot
		plt.subplot(nr,nc,i*2+1)
		plt.plot(range(n), e)
		plt.ylabel('move='+str(move_type)+' energy')
		plt.subplot(nr,nc,i*2+2)
		plt.plot(range(n), move)
		plt.ylabel('move')
	
	plt.show()

			
def test_AB_SFC_macro1_cpp_wl_compare_ub():
	"""
	compare effect of upper bound
	move_type = [0,1,2]
	upper_bound = [50,100,150]
	
	plot as a function of iteration :
	- energy, matrix, graph, other indicators (graph distance?)
	Compare several scenarii:  
	- raw sampling no energy constraint)
	- wl with energy constraint, with != types of moves
	"""
	# prepare system
	nf = 20
	nh = 100
	density = 0.05
	print "Generate dataset"
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced',nf=nf,nh=nh,density=density)
	# prepare simulation
	move_type = 0
	niter = 10000000
	nsamp =100
	do_copy =  int(float(niter)/float(nsamp))
	#   histo
	bins=30
	finit=1.0 ; fmin= 0.0001	
	lb=7
	ub_vec=[50,100,150]
	ub_max = 1000
	# start simulation
	print "Start sampling:"
	for i,ub in enumerate( ub_vec  ):
		#
		A_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)
		C_sample = scipy.sparse.csr_matrix((nsamp*nf,nh),dtype=float)	
		ener_sample = np.zeros(nsamp)
		count = np.zeros(1,dtype=int)
		count_max=nsamp
		f = utils.get_copy_func_AB_SFC_macro1(A_sample,C_sample,ener_sample,
			nf,nh,count,count_max )
		#
		sys=wlmcmc.AB_SFC_macro1_cpp(nf,nh,A,C)
		# sample
		lnh = wlmcmc.Histogram(lb,ub, bins)
		lnh,f_end = wlmcmc.wl(sys, lnh, finit=finit,fmin = fmin,niter=niter,do_copy=do_copy,
			copy_lb=lb, copy_ub=ub_max,copy_func=f,
			flatness=0.80, verbose=2, move_type=move_type)
		# get energy ?
		plt.subplot(3,1,i+1)
		plt.plot(ener_sample)
	# get distance ?
	#%matplotlib
	plot_lnh(lnh)
	plt.matshow(A_sample.toarray())
			
#################	
def test_AB_SFC_macro1(self):
	"""
	"""
	A = scipy.sparse.csr_matrix([[1,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	ab._print_state()
	
def test_AB_SFC_macro1_balance(self):
	"""
	"""
	A = scipy.sparse.csr_matrix([[2.,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	ab._balance_hh()
	ab._balance_firm()
	ab._print_state()
	# expected balance_hh  [-1,0,-1]
	# expected balance_firm  [2,0]

def test_copy():
	"""
	"""
	A = scipy.sparse.csr_matrix([[1,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	A_=scipy.sparse.csr_matrix((nf,nh))
	C_=scipy.sparse.csr_matrix((nf,nh))
	def get_func(_A,_C):
		def f(a,c):
			a=_A
			c=_C
		return  f
	f= get_func(A,C)		
	f(A_,C_)
	assert A==A_

def test_AB_SFC_macro1_copy_state():	
	"""
	"""
	A = scipy.sparse.csr_matrix([[1,0,1],[0,1,0] ])
	C = scipy.sparse.csr_matrix([[1,0,0],[0,1,0] ])
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	def get_func(_A,_C):
		if not scipy.sparse.isspmatrix_lil(_A): raise TypeError('_A must be lil')
		if not scipy.sparse.isspmatrix_lil(_C): raise TypeError('_C must be lil')
			
		def f(a_topo,a_weight,c_topo,c_weight):
			for i,val in enumerate(a_topo): _A.rows[i]=val
			for i,val in enumerate(a_weight): _A.data[i]=val
			for i,val in enumerate(c_topo): _C.rows[i]=val
			for i,val in enumerate(c_weight): _C.data[i]=val
		return  f	
	f= get_func(A_,C_)
	ab.copy_state(f)
	assert np.all((A.tolil()==A_).toarray())
	assert np.all((C.tolil()==C_).toarray())
	

def test_AB_SFC_macro1__swap_cons_hh_degree():
	"""
	check _swap_cons_hh_degree
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	ab=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	ab._print_state()
	move_done = ab._swap_cons_hh_degree()		
	ab._print_state()
	# copy state
	f= utils.get_func_AB_SFC_macro1(A_,C_)
	ab.copy_state(f)
	# check that C doesn't change
	assert np.all((C.tolil()==C_).toarray())
	# check that A is just a permutation
	assert utils.check_permutation(A,A_)



def test_AB_SFC_macro1__swap_cons_hh_degree_rollback(self):
	"""
	visually check that delta and energy goes back to initial state after rollback
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	f= utils.get_func_AB_SFC_macro1(A_,C_)
	sys=wlmcmc.AB_SFC_macro1(nf,nh,A,C)	
	# propose move
	move_done=0
	while move_done==0: move_done=sys.propose_move(1)
	# compare state: must have changed
	sys.copy_state(f)
	assert not np.all((A.tolil()==A_).toarray())
	sys.rollback(move_done)
	sys.copy_state(f)
	# compare state: must have returned
	assert np.all((A.tolil()==A_).toarray())
		
def test_AB_SFC_macro1__swap_cons_hh_degree_rollback_visual(self):
	"""
	visually check that delta and energy goes back to initial state after rollback
	"""
	A,C = dataset.get_AB_SFC_macro1(dataset='base')
	nf = A.shape[0]
	nh = A.shape[1]
	sys=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	#
	print "compute energy=",sys.get_energy() 
	sys._print_state()
	move_done = 0
	while move_done==0: move_done=sys.propose_move(1)
	sys.get_energy() ; sys._print_state()
	if(True): sys.rollback(move_done); print "rollback"
	sys.get_energy() ;sys._print_state()	

def test_AB_SFC_macro1_get_linear_system():
	#%matplotlib
	nf = 5
	nh = 30
	o = scipy.sparse.csr_matrix((nf,nh))
	sys=wlmcmc.AB_SFC_macro1(nf,nh,o,o)
	F,g=sys.get_linear_system()	
	plt.matshow(F.toarray(), aspect="auto")

def test_AB_SFC_macro1_dataset():
	# check
	A,C =dataset.get_AB_SFC_macro1(dataset='balanced')
	assert np.allclose( A.sum(axis=0), C.sum(axis=0))
	assert np.allclose( A.sum(axis=1), C.sum(axis=1))
	
	# check
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced_sparse')
	assert np.allclose( A.sum(axis=0), C.sum(axis=0))
	assert np.allclose( A.sum(axis=1), C.sum(axis=1))
	# plot
	#%matplotlib
	plt.subplot(2,1,1)
	plt.matshow(A, aspect="auto",fignum=False)	
	plt.subplot(2,1,2)
	plt.matshow(C, aspect="auto",fignum=False)	
	
def test_AB_SFC_macro1_WL():
	A,C = dataset.get_AB_SFC_macro1(dataset='balanced')
	nf = A.shape[0]
	nh = A.shape[1]
	A_=scipy.sparse.lil_matrix((nf,nh))
	C_=scipy.sparse.lil_matrix((nf,nh))
	f= get_func_AB_SFC_macro1(A_,C_)
	sys=wlmcmc.AB_SFC_macro1(nf,nh,A,C)
	move_type = 1
	
	count_max = 10000
	bins=30
	finit=1.0 ; fmin= 0.0001
	lb,ub=0,5
	lnh = wlmcmc.Histogram(lb,ub, bins)
	
	lnh,f_end = wlmcmc.wl(sys, lnh, finit=finit,fmin = fmin,niter=3000000,do_copy=10000,
	   copy_lb=lb, copy_ub=ub,copy_func=f,
	   flatness=0.80, verbose=2, move_type=move_type)
	#%matplotlib
	plot_lnh(lnh)

def test_AB_SFC_macro1__swap_firm_degree():
	"""
	NOT IMPLEMENTED YET
	check _swap_firm_degree
	"""		
	pass
		
	
################
def test_wl_GraphCSR():
	"""
	"""	
	pass
	
def test_wl_mc():	
	pass

def test_wl_simulate_net():
	"""
	DEPRECATED
	"""	
	gCSR0,lnh,p = dataset.get_gCSR_lnh(data='x0_nls_L1')
	#gCSR0,lnh = get_gCSR_lnh(dataset='base')
	#gCSR0,lnh = get_gCSR_lnh(dataset='x0_nls')
	# ---------------
	# WL
	bins=p['hist']['bins']
	nsteps=p['sampler']['nsteps']
	finit=p['sampler']['finit']
	fmin =p['sampler']['fmin'] 
	rt = p['sampler']['nround_trips']
	lowerBound = p['hist']['lb']
	upperBound = p['hist']['ub']
	print "start wl..."
	lnh = wlmcmc.wl_simulate_net(gCSR0,lnh,finit=finit, fmin=fmin,round_trips=rt,
			copy_nmax=-1, copy_lb=-0.1, copy_ub=0.1,copy_func=None	)
	# ---------------							
	# plot
	plot_lnh(lnh)
	# --------------
	# MCMC
	# marginal: 
	nsteps, nsamples= 100, 1000
	print "start mc..."
	marginal, sample = wlmcmc.mc_simulate_net(gCSR0,lnh, nsteps, nsamples)
	

def test_wl_simulate_net_copy():
	"""
	test wl_simulate_net + copying with utils.get_copy_func
	
	DEPRECATED
	
	"""
	
	#gCSR0,lnh,p = get_gCSR_lnh(data='x0_nls')
	gCSR0,lnh,p = dataset.get_gCSR_lnh(data='x0_nls_L1')
	#----------------
	# WL + copy states
	# init
	bins=p['hist']['bins']
	nsteps=p['sampler']['nsteps']
	finit=p['sampler']['finit']
	fmin =p['sampler']['fmin'] 
	rt = p['sampler']['nround_trips']	
	nr_x0=p['system']['nr_x0']
	nc_x0=p['system']['nc_x0']
	lowerBound = p['hist']['lb']
	upperBound = p['hist']['ub']

	nsamp=30
	samples_csr = scipy.sparse.csr_matrix((nsamp*nr_x0,nc_x0),dtype=float)
	ener = np.zeros(nsamp)
	count = np.zeros(1,dtype=int)
	# copy func
	#f= utils.get_copy_func(samples_csr ,ener, nr_x0,nc_x0)
	f= utils.get_copy_func(samples_csr ,ener, nr_x0,nc_x0,count,nsamp)	
	# run wl
	#lnh = wlmcmc.wl_simulate_net(gCSR0,lnh,finit=finit, fmin=fmin,round_trips=rt,copy_nmax=nsamp, copy_lb=-0.1, copy_ub=0.1,copy_func=f	)
	lnh = wlmcmc.wl_simulate_net(gCSR0,lnh,finit=finit, fmin=fmin,round_trips=rt,do_copy=True, copy_lb=-0.1, copy_ub=0.1,copy_func=f	)
	# plot sample: block
	plt.hist(np.array(samples_csr[0:nr_x0,:].todense()).flatten())
	# plot sample: column marginal
	plt.hist(np.array(samples_csr.getcol(0).todense()).flatten())
	# compare sparsity rate to 
	utils.plot_sparsity(samples_csr, nr_x0, nc_x0,count[0], sparsity0=p['system']['sparsity_x0'])
	# plot energy
	plt.plot(ener[0:count[0]])
	plt.hist(ener[0:count[0]])
	# save sparse matrix samples_csr
	



# --------------
# TESTS 

# dataset.py
#   check x0>0
#   check sparsity x0

# Histogram:
# check binning!!!!!!!!!!!!!!
#        pouqruoi le bin 0 n'est jamais atteint avec les énerg négatives ?
#        todo? raz histo ?

# GraphCSR
# propose_move_degree
#   check at each node: degree constant
#   check local consistency of Ax-b
# propose_move_weight:
#   check at each node:  w_i constant  
# check energy 
# check rollback
# diagnose rejection: both moves equally rejected ?

# WL:
# plusieurs graines de x0? (cf wang qui découpe [Elow,Ehigh] en ss-interv
#       permettrait aussi de limiter accumulation erreur numériq (+-)
# diagnose: is detailed balance condition verified ?
# comparer avec résultat analytiq: 
#              ising: cf papier original wang-landau ; 
#                     ising cython: wangLand.py, http://jakevdp.github.io/blog/2017/12/11/live-coding-cython-ising-model/
#              graph  

# samples:
#   [OK] check x0>0
#   [OK] check sparsity x0, xi
#   degree,...
#   "correlation" between samples ??

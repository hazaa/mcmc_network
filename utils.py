# -*- coding: utf-8 -*- 
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse

def distance(A1,A2):
	# idee: sum(A1*A2) / nnz
	# impleme efficace ?
	pass
	
def check_permutation(A1,A2):
	"""
	check that each column A1[:,i] is a permutation of 2 elems of A2[:,i]
	usage: see AB_SFC_macro1
	
	input:
	-------
	A1: array, shape=(m,n)
	
	A2: array, shape=(m,n)
	
	"""
	# check inputs
	if scipy.sparse.isspmatrix(A1):
		A1 = A1.toarray()
	if scipy.sparse.isspmatrix(A2):
		A2 = A2.toarray()
	if not A1.shape==A2.shape:
		raise ValueError('A1 and A2 have different shapes')
	nc = A1.shape[1]
	nbcol=0
	# for each column
	for j in range(nc):
		col_1 = A1[:,j]
		col_2 = A2[:,j]
		if not np.all(col_1==col_2):
			#   first col differs by just two entries
			if np.sum((col_1!=col_2)*1) !=2: 
				print "1",col_1,col_2
				return 0
			#   set of values in first row is invariant
			if set( col_1.tolist() ) !=set( col_2.tolist() ):
				print "2",col_1,col_2
				return 0
			nbcol+=1 	
	if nbcol>2: 
		raise ValueError('more than 2 column have changed')	
	return 1

def get_func_AB_SFC_macro1(_A,_C):
	
	#copies the state once to a lil matrix
	
	if not scipy.sparse.isspmatrix_lil(_A): raise TypeError('_A must be lil')
	if not scipy.sparse.isspmatrix_lil(_C): raise TypeError('_C must be lil')
	def f(a_topo,a_weight,c_topo,c_weight):
		for i,val in enumerate(a_topo): _A.rows[i]=val
		for i,val in enumerate(a_weight): _A.data[i]=val
		for i,val in enumerate(c_topo): _C.rows[i]=val
		for i,val in enumerate(c_weight): _C.data[i]=val
	return  f

"""def get_copy_func_AB_SFC_macro1(A,C,nr,nc,count,count_max ):

	def f(a_topo,a_weight,c_topo,c_weight):

		# copy the array y into array x, and float e into ener
		if count[0]<count_max:
			# 
			_A=scipy.sparse.lil_matrix((nr,nc))
			_C=scipy.sparse.lil_matrix((nr,nc))
			#
			for i,val in enumerate(a_topo): _A.rows[i]=val
			for i,val in enumerate(a_weight): _A.data[i]=val
			for i,val in enumerate(c_topo): _C.rows[i]=val
			for i,val in enumerate(c_weight): _C.data[i]=val 
			# copy
			#print "count=",count[0]
			#print "shape A:", A.shape
			#print "shape:",A[count[0]*nr:(count[0]+1)*nr, :].shape
			#print "shape:",_A.tocsr().shape
			A[count[0]*nr:(count[0]+1)*nr, :] = _A.tocsr()
			A.eliminate_zeros() # doesn't change the internal state y
			C[count[0]*nr:(count[0]+1)*nr, :] = _C.tocsr()
			C.eliminate_zeros() # doesn't change the internal state y
			#ener[i]=e
			# increment
			count[0] = count[0] +1	
	return f"""


def get_copy_func_AB_SFC_macro1(A,C,energy,nr,nc,count,count_max ):
	""" 
	copies several copies to a csr matrix
	
	function that returns a parametrized function
	 (can be done with functools.partial)
	input:
	------
	x: array, shape=()
	
	ener: array, shape=()
	
	count: array, shape=()
	
	nr: int
	
	nc: int
	
	count: array, shape=()
	
	output:
	-------
	myfunc: function
	
	"""
	
	def f(a_topo,a_weight,c_topo,c_weight,e):

		# copy the array y into array x, and float e into ener
		if count[0]<count_max:
			# 
			_A=scipy.sparse.lil_matrix((nr,nc))
			_C=scipy.sparse.lil_matrix((nr,nc))
			#
			for i,val in enumerate(a_topo): _A.rows[i]=val
			for i,val in enumerate(a_weight): _A.data[i]=val
			for i,val in enumerate(c_topo): _C.rows[i]=val
			for i,val in enumerate(c_weight): _C.data[i]=val 
			# copy
			#print "count=",count[0]
			#print "shape A:", A.shape
			#print "shape:",A[count[0]*nr:(count[0]+1)*nr, :].shape
			#print "shape:",_A.tocsr().shape
			A[count[0]*nr:(count[0]+1)*nr, :] = _A.tocsr()
			A.eliminate_zeros() # doesn't change the internal state y
			C[count[0]*nr:(count[0]+1)*nr, :] = _C.tocsr()
			C.eliminate_zeros() # doesn't change the internal state y
			energy[count[0]]=e
			# increment
			count[0] = count[0] +1	
	return f

def get_copy_func_old(x,ener,nr,nc ):
	""" function that returns a parametrized function
	 (can be done with functools.partial)
	input:
	------
	x: array, shape=()
	
	ener: array, shape=()
	
	count: array, shape=()
	
	nr: int
	
	nc: int
	
	output:
	-------
	myfunc: function
	
	"""
	
	def myfunc(y,e,i):
		# copy the array y into array x, and float e into ener
		x[i*nr:(i+1)*nr, :] = y
		ener[i]=e
		x.eliminate_zeros() # doesn't change the internal state y
	return myfunc

def get_copy_func_ising(ener,count,count_max ):
	""" function that returns a parametrized function
	 (can be done with functools.partial)
	input:
	------
	x: array, shape=()
	
	ener: array, shape=()
	
	count: array, shape=()
	
	nr: int
	
	nc: int
	
	count: array, shape=()
	
	output:
	-------
	myfunc: function
	
	"""
	
	def myfunc(e):
		# copy the array y into array x, and float e into ener
		i=count[0]
		if i<count_max:
			#x[i*nr:(i+1)*nr, :] = y
			ener[i]=e
			# increment
			count[0] = count[0] +1
		else:
			ener[0]=e
			count[0] =	1
	return myfunc

def get_copy_func(x,ener,nr,nc,count,count_max ):
	""" function that returns a parametrized function
	 (can be done with functools.partial)
	input:
	------
	x: array, shape=()
	
	ener: array, shape=()
	
	count: array, shape=()
	
	nr: int
	
	nc: int
	
	count: array, shape=()
	
	output:
	-------
	myfunc: function
	
	"""
	
	def myfunc(y,e):
		# copy the array y into array x, and float e into ener
		i=count[0]
		if i<count_max:
			x[i*nr:(i+1)*nr, :] = y
			ener[i]=e
			x.eliminate_zeros() # doesn't change the internal state y
			# increment
			count[0] = count[0] +1
	return myfunc

def plot_sparsity(samples, nr_x0, nc_x0, nsamp, sparsity0=0.0):
	s = np.zeros(nsamp)
	for i in range(nsamp):
		s[i]=samples[i*nr_x0:(i+1)*nr_x0].nnz / float(nr_x0*nc_x0)
	plt.plot( range(nsamp),s,'-o', [0,nsamp-1],[sparsity0,sparsity0], 'red')
	plt.show()

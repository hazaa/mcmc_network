MCMC network sampler
======

Several Markov Chain Monte-Carlo samplers for weighted (possibly multilayer)
networks that arise in socio-economic applications where local conservation
os some quantity (e.g. money) may hold.

*   Wang-Landau (WL) sampler for networks, following (Fischer et al.15).
*   A swap + nnls sampler. 

## How to install

*   get source: 

        git clone https://gitlab.com/hazaa/mcmc_network.git
        
*   dependencies:
    *    python2.7, numpy, cython
    *    dataset.py depends on https://gitlab.com/hazaa/sfc_proba

## How to compile

*    python setup.py build_ext --inplace


Usage
------

see:

*   /notebooks
*   /tests


References
------

WL+graph: 

*    Fischer et al. arXiv:1507.08696
*    Ansmann et al. http://dx.doi.org/10.1103/PhysRevE.84.026103
*    Basler et al. https://doi.org/10.1093/bioinformatics/btr145
*    Cimini et al. arXiv:1810.05095
*    Roberts et al. https://journals.aps.org/pre/abstract/10.1103/PhysRevE.85.046103

WL sampling:

*    Wang, Landau http://arXiv.org/abs/cond-mat/0011174v1
*    Landau, Binder, "A Guide to Monte Carlo Simulations in Statistical Physics", §7.8, 2014.

WL sample codes:

*    https://github.com/SamplingConstrainedNetworks/
*    http://www.physics.rutgers.edu/~haule/681/src_MC/python_codes/

Others:

*    hmc hamiltonian monte-carlo
*    theory about nnls solvers: http://dx.doi.org/10.1142/9789812836267_0008, http://users.wfu.edu/plemmons/papers/Chennnonneg.pdf
*    nnls sample codes: http://maggotroot.blogspot.com/2013/11/constrained-linear-least-squares-in.html 
*    fast nnls theory : https://www.researchgate.net/publication/230554373_A_Fast_Non-negativity-constrained_Least_Squares_Algorithm
*    fast nnls code:  https://rdrr.io/cran/multiway/man/fnnls.html, https://sourceforge.net/p/matplotlib/mailman/message/12454187/

TODO
------

*    add weight swap in same bin of a histogram.

# -*- coding: utf-8 -*-
#cython: boundscheck=False, wraparound=False, nonecheck=False
"""
Adapted from:
[Fischer et al 15] Sampling motif-constrained ensembles of networks, by R. Fischer, J. C. Leitão, T. P. Peixoto and E. G. Altmann.
https://github.com/SamplingConstrainedNetworks/


http://cython.readthedocs.io/en/latest

COMPILE

python setup.py build_ext --inplace

or 

>cython wl_mcmc_network.pyx -a
>gcc -O3 -march=native wl_mcmc_network.c -shared -fPIC `python-config --cflags --libs` -o wl_mcmc_network.so

TODO:
* histogram for int or double

"""
import math
import numpy as np
cimport numpy as np

#from scipy.sparse import csr_matrix, isspmatrix, isspmatrix_csr,rand,vstack,hstack
import scipy.sparse

ctypedef np.int32_t cINT32
ctypedef np.double_t cDOUBLE

from libc.math cimport exp     # ising
from libc.math cimport abs as cabs
from libc.stdlib cimport rand # ising
cdef extern from "limits.h":  # ising
    int RAND_MAX
cdef extern from "stdlib.h":
    double drand48()
    void srand48(long int seedval)

cdef extern from "time.h":
    long int time(int)

# distutils: language = c++
from libcpp.vector cimport vector
cdef extern from "<algorithm>" namespace "std":
     iter std_find "std::find" [iter, T](iter first, iter last, const T& val)
     int std_distance "std::distance" [iter](iter first, iter last)

cdef class TestCopyFunc(object):
    """
    class to test a copy mechanism using a python function
    """
    cdef int [:]  x0_indices, x0_indptr
    cdef double [:] x0_data	   
    cdef int nr_A,nc_A,nr_x0,nc_x0
    def __init__(self, np.ndarray[np.double_t] x0_data,
                 np.ndarray[int] x0_indices,
                 np.ndarray[int] x0_indptr,int nr_x0, int nc_x0,):
       self.x0_data=x0_data				   
       self.x0_indptr=x0_indptr
       self.x0_indices=x0_indices 
       self.nr_x0=nr_x0						
       self.nc_x0=nc_x0   	
    def test_copy_func(self,int i,f):
        f(scipy.sparse.csr_matrix((self.x0_data, self.x0_indices, self.x0_indptr),
                  shape=(self.nr_x0,self.nc_x0 )) ,i )		

cdef class Ising(object):
    """
    usage:
    isi = Ising(..)
    isi._compute_energy() # initial
    isi.propose_move(dummy) # for compatibility
    isi.get_energy()
    if(...): isi.rollback()
    
    
    Refs:
    wangLand.py, K. Haule http://www.physics.rutgers.edu/~haule/681/src_MC/python_codes/
    [jakevdp] http://jakevdp.github.io/blog/2017/12/11/live-coding-cython-ising-model/
    
    
    field = np.random.choice([-1, 1], size=(N, M))
    """	
    cdef int N,M,NM,n_old,m_old
    #cdef int [:,:] field
    cdef long [:,:] field
    cdef float dE 
    cdef float E 	   				          
    def __init__(self, field, int N, int M):
       self.field = field
       self.N = N
       self.M=M
       self.NM=N*M
       self.dE = 0.
       self.E = 0.
    def _get_dE(self )	:   
       return self.dE   
    def get_energy(self )	:   
       return self.E
    def _set_seed(self,int t):
        """
        reset the seed, for reproducibility.
        
        input: 
        ------
        t : int
        the seed
        """
        srand48(t)   		
    def update_energy(self,int dum)	:
       """
       input:
       ------
       dum: int
       not used. 

       NB: i % self.N enforces the toroidal topology
       !function has confusing name, should be named 'update_energy', or delta_E !!!!!!!!!!!!!
	   """
       cdef int total = 0
       # compute dE after proposed move
       cdef int i, j
       total = self.field[(self.n_old+1)%self.N, self.m_old] + self.field[self.n_old,(self.m_old+1)%self.M] + self.field[(self.n_old-1)%self.N,self.m_old] + self.field[self.n_old,(self.m_old-1)%self.M]
       """for i in range(self.n_old-1, self.n_old+2):
           for j in range(self.m_old-1, self.m_old+2):
               if i == self.n_old and j == self.m_old:
                   pass
               else: total += self.field[i % self.N, j % self.M]"""
       # NB: below, dE is the opposite of the usual formula.
       # This is because the spin has been flipped first (in propose_move)        
       self.dE = -2 * self.field[self.n_old, self.m_old] * total   
       # update energy
       self.E = self.E + self.dE
       return self.E
    def propose_move(self,int dum):	
       """
       input:
       ------
       dum: int
       not used. 
       """	
       cdef int n,m		
       cdef int i=int(drand48()*self.NM) 
       #np.random.randint(self.N*self.M)		
       #n=np.random.randint(self.N)		
       #m=np.random.randint(self.M)
       n = i % self.N
       m = i / self.N
       #n = int(drand48()*self.N)
       #m = int(drand48()*self.M)
       self.field[n,m] *= -1	 	
       #record for rollback 
       self.n_old = n
       self.m_old = m
       return 1 # for compatibility       
    def rollback(self,dum):	 # for compatibility
       # rollback spin
       self.field[self.n_old,self.m_old] *= -1       
       # rollback energy
       self.E = self.E - self.dE  
    def copy_state(self,f):
       """
       use the supplied function f to copy the current state 
       """
       #f(csr_matrix((self.x0_data, self.x0_indices, self.x0_indptr), shape=(self.nr_x0,self.nc_x0 )) , self.get_energy(0), i )
       f(self.get_energy() )     
    def _compute_energy(self):
       """Energy of a 2D Ising lattice
       """
       self.E = 0.
       cdef int i,j,WF
       #cdef int ii,jj 
       cdef int N = self.N
       cdef int M = self.M
       for i in range(N):
           for j in range(M):
               WF = self.field[(i+1)%N, j] + self.field[i,(j+1)%N] + self.field[(i-1)%N,j] + self.field[i,(j-1)%N]
               self.E -= WF * self.field[i,j]          
       self.E =  self.E/2.  
    def _print_state(self):
       print 'E=',self.E, ' dE=',self.dE, 'n_old=',self.n_old ,' m_old=',self.m_old		   

cdef class TestStdCpp(object):
    """
    test some structures and algorithms of the c++ std library in cython
    """		       
    cdef vector[vector[int]] cons_hh_topo	
    cdef vector[vector[int]] cons_firm_topo
    def __init__(self):
       """
       """		       
       cdef vector[int] vect_int 
       #cdef list l = [1,0,1]
       # A = [[1,0,1],[0,1,0]]
       self.cons_hh_topo = [[0, 2], [1, 2], [0, 2], [2]]
       self.cons_firm_topo= [[0, 2], [1], [0, 1, 2, 3]]
       #for i in range(0,self.nh):		   
       #   self.cons_hh_topo.push_back(vect_int) 
    def test_find(self):
       """
       find an element in a vector
       """
       cdef int fi2,hh1 = 0
       #cdef int fi2 = 2
       cdef list l = [2,10]
       for fi2 in l:
		  # see class AB_SFC_macro1_cpp._swap_cons_hh_degree()
          if self.cons_hh_topo[hh1].end()==std_find[vector[int].iterator, int](self.cons_hh_topo[hh1].begin(), self.cons_hh_topo[hh1].end(), fi2)	:	       		
             print 'element:',fi2, ' not found in:',self.cons_hh_topo[hh1]
          else:    
             print 'element:',fi2, ' was found in:',self.cons_hh_topo[hh1] 
		  
    def test_index(self):	   
       """
       find the position of an element in a vector
       see class AB_SFC_macro1_cpp._swap_cons_hh
       """
       cdef int hh1 = 3
       cdef int fi1 = 2
       cdef int idx1
       idx1 = std_distance[vector[int].iterator](self.cons_firm_topo[fi1].begin() , \
               std_find[vector[int].iterator, int](self.cons_firm_topo[fi1].begin(), self.cons_firm_topo[fi1].end(), hh1) )                                  					
       print 'element:',hh1, ' was found at position:', idx1,' in:',self.cons_firm_topo[fi1]
		       		
    def _print_state(self):
       print "-------"   
       print "self.cons_hh_topo=",self.cons_hh_topo	
   

cdef class AB_SFC_macro1_cpp(object):
    """
    https://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html#standard-library
    http://www.cplusplus.com/reference/vector/vector/
    https://www.tutorialspoint.com/cpp_standard_library/cpp_algorithm_find.htm
    https://stackoverflow.com/questions/30943692/removing-an-element-from-stdvector-in-cython
    """	
    cdef int nf,nh
    cdef vector[vector[int]] cons_hh_topo
    cdef vector[vector[double]] cons_hh_weight
    cdef vector[vector[int]] cons_firm_topo
    cdef vector[vector[double]] cons_firm_weight
    cdef vector[vector[int]] wage_hh_topo
    cdef vector[vector[double]] wage_hh_weight
    cdef vector[vector[int]] wage_firm_topo
    cdef vector[vector[double]] wage_firm_weight
    cdef double [:] balance_hh
    cdef double [:] balance_firm
    cdef int hh1_old, hh2_old,idx1_old,idx2_old,fi1_old,fi2_old # for rollback
    def __init__(self,int nf,int nh, A, C):
       """
       input:
       ------
       A: sparse array, shape = (nf,nh)
       consumption paid to firms by hh. Values in IR+
       
       C: sparse array, shape = (nf,nh)
       wage paid to hh by firms. Values in IR+
       """		
       self.nf = nf
       self.nh = nh
       self.balance_hh=np.zeros(nh,dtype=float)
       self.balance_firm=np.zeros(nf,dtype=float)
       if not scipy.sparse.isspmatrix_csr(A):
          raise TypeError('A must be csr matrix') 
       if not scipy.sparse.isspmatrix_csr(C):
          raise TypeError('C must be csr matrix') 	  
       if not A.shape==(nf,nh):
          raise ValueError('A:invalid size')
       if not C.shape==(nf,nh):
          raise ValueError('C:invalid size')
       self._init(A,C) 
    def _init(self,A,C):   
       """
       input:
       ------
       A: sparse csr array, shape = (nf,nh)
       consumption paid to firms by hh
       
       C: sparse csr array, shape = (nf,nh)
       wage paid to hh by firms
       """	
       cdef int i,k	
       cdef double x
       cdef list l_topo,l_weight
       cdef vector[int] vect_int
       cdef vector[double] vect_double
       # fill cons_hh_topo and cons_hh_weight
       for i in range(0,self.nh):		   
          self.cons_hh_topo.push_back(vect_int)
          self.cons_hh_weight.push_back(vect_double)
       for i in range(0,self.nh):
          l_topo   =A.getcol(i).tocsc().indices.tolist()
          l_weight = A.getcol(i).tocsc().data.tolist()  
          for k in l_topo:
             self.cons_hh_topo[i].push_back( k )				
          for x in l_weight:
             self.cons_hh_weight[i].push_back( x )   
       # fill cons_firm_topo and cons_firm_weight 
       for i in range(0,self.nf):     
          self.cons_firm_topo.push_back(vect_int)
          self.cons_firm_weight.push_back(vect_double)
       for i in range(0,self.nf):
          l_topo   = A.getrow(i).indices.tolist()
          l_weight = A.getrow(i).data.tolist()
          for k in l_topo:
             self.cons_firm_topo[i].push_back( k )				
          for x in l_weight:
             self.cons_firm_weight[i].push_back( x )   
       # fill wage_hh_topo and wage_hh_weight
       for i in range(0,self.nh):     
          self.wage_hh_topo.push_back(vect_int)
          self.wage_hh_weight.push_back(vect_double)
       for i in range(0,self.nh):
          l_topo   = C.getcol(i).tocsc().indices.tolist() 
          l_weight = C.getcol(i).tocsc().data.tolist()
          for k in l_topo:
             self.wage_hh_topo[i].push_back( k )				
          for x in l_weight:
             self.wage_hh_weight[i].push_back( x ) 
       # fill wage_firm_topo and wage_hh_weight
       for i in range(0,self.nf):     
          self.wage_firm_topo.push_back(vect_int)
          self.wage_firm_weight.push_back(vect_double)       
       for i in range(0,self.nf):
          l_topo   = C.getrow(i).indices.tolist() 
          l_weight = C.getrow(i).data.tolist()
          for k in l_topo:
             self.wage_firm_topo[i].push_back( k )				
          for x in l_weight:
             self.wage_firm_weight[i].push_back( x )            		         
    cdef _balance_hh(self):		
       """
       compute the balance of households
       """
       cdef int i
       cdef double x,s
       for i in range(self.nh): 
          s = 0.
          for x in self.cons_hh_weight[i]: s-=  x
          for x in self.wage_hh_weight[i]: s+=  x
          self.balance_hh[i] = s
    cdef _balance_firm(self): 
       """
       compute the balance of firms
       """
       cdef int i
       cdef double x,s
       for i in range(self.nf): 
          s = 0. 
          for x in self.cons_firm_weight[i]: s+=  x
          for x in self.wage_firm_weight[i]: s-=  x
          self.balance_firm[i] =  s
          
    cdef _swap_wage_firm(self, int fi1, int idx1,int fi2, int idx2):                              
       """
       """          
       cdef int hh1,hh2, len1, len2,dum
       cdef double w
       cdef double w1,w2
       # exchange indices in wage_firm_topo 
       hh1=self.wage_firm_topo[fi1][idx1]
       self.wage_firm_topo[fi1][idx1] = self.wage_firm_topo[fi2][idx2]
       self.wage_firm_topo[fi2][idx2] = hh1
       hh2=self.wage_firm_topo[fi1][idx1]
       # exchange indices in cons_hh_weight 
       w1 = self.wage_firm_weight[fi1][idx1]
       self.wage_firm_weight[fi1][idx1] = self.wage_firm_weight[fi2][idx2]
       self.wage_firm_weight[fi2][idx2] = w1
       w2=self.wage_firm_weight[fi1][idx1]
       #
       idx1 = std_distance[vector[int].iterator](self.wage_hh_topo[hh1].begin() , \
               std_find[vector[int].iterator, int](self.wage_hh_topo[hh1].begin(), self.wage_hh_topo[hh1].end(), fi1) )                                  					
       idx2 = std_distance[vector[int].iterator](self.wage_hh_topo[hh2].begin() , \
               std_find[vector[int].iterator, int](self.wage_hh_topo[hh2].begin(), self.wage_hh_topo[hh2].end(), fi2) ) 
       #               
       self.wage_hh_topo[hh1][idx1]=fi2
       self.wage_hh_topo[hh2][idx2]=fi1
       w=self.wage_hh_weight[hh1][idx1]
       self.wage_hh_weight[hh1][idx1] = self.wage_hh_weight[hh2][idx2]
       self.wage_hh_weight[hh2][idx2]=w
       
    cdef _swap_cons_hh(self, int hh1, int idx1,int hh2, int idx2):                              
       """
       """
       cdef int fi1,fi2, len1, len2,dum
       cdef double w
       cdef double w1,w2
       # exchange indices in cons_hh_topo 
       fi1=self.cons_hh_topo[hh1][idx1]
       self.cons_hh_topo[hh1][idx1] = self.cons_hh_topo[hh2][idx2]
       self.cons_hh_topo[hh2][idx2] = fi1
       fi2=self.cons_hh_topo[hh1][idx1]
       # exchange indices in cons_hh_weight 
       w1 = self.cons_hh_weight[hh1][idx1]
       self.cons_hh_weight[hh1][idx1] = self.cons_hh_weight[hh2][idx2]
       self.cons_hh_weight[hh2][idx2] = w1
       w2=self.cons_hh_weight[hh1][idx1]
       # exchange: in conso_firm_topo[fi1]/weight: remove hh1,add hh2
       #           in conso_firm_topo[fi2]/weight: remove hh2,add hh1
       # remove weight
       #idx1 = self.cons_firm_topo[fi1].index(hh1)
       #idx2 = self.cons_firm_topo[fi2].index(hh2)       
       idx1 = std_distance[vector[int].iterator](self.cons_firm_topo[fi1].begin() , \
               std_find[vector[int].iterator, int](self.cons_firm_topo[fi1].begin(), self.cons_firm_topo[fi1].end(), hh1) )                                  					
       idx2 = std_distance[vector[int].iterator](self.cons_firm_topo[fi2].begin() , \
               std_find[vector[int].iterator, int](self.cons_firm_topo[fi2].begin(), self.cons_firm_topo[fi2].end(), hh2) ) 
       #print "idx2=",idx2, " self.cons_firm_weight[fi2]=",self.cons_firm_weight[fi2]
       self.cons_firm_topo[fi1][idx1]=hh2
       self.cons_firm_topo[fi2][idx2]=hh1
       w=self.cons_firm_weight[fi1][idx1]
       self.cons_firm_weight[fi1][idx1] = self.cons_firm_weight[fi2][idx2]
       self.cons_firm_weight[fi2][idx2]=w
    cdef _swap_wage_firm_degree(self):
       """
       swap wage link between firms and hh, preserving degree on firm side.
       Weight is preserved on firm side but not on hh side.
       """		 
       cdef int hh1,hh2,fi1,fi2,len1,len2
       cdef int idx1,idx2 
       # sample
       fi1=int(drand48()*self.nf) 
       fi2=int(drand48()*(self.nf-1)) 
       if fi2>=fi1: fi2+= 1 
       #
       len1 = self.wage_firm_topo[fi1].size() 
       len2 = self.wage_firm_topo[fi2].size() 
       if len1>0 and len2>0:
          # random indices 
          #idx1 = np.random.randint(len1)        			  
          #idx2 = np.random.randint(len2)  
          idx1 = int(drand48()*len1)
          idx2 = int(drand48()*len2)
          hh1=self.wage_firm_topo[fi1][idx1]
          hh2=self.wage_firm_topo[fi2][idx2]
          # check it's not the same hh
          if hh1==hh2:
             return 0			      
          # check hh is not already in list 
          if self.wage_firm_topo[fi1].end()!=std_find[vector[int].iterator, int](self.wage_firm_topo[fi1].begin(), self.wage_firm_topo[fi1].end(), hh2):
             return 0
          if self.wage_firm_topo[fi2].end()!=std_find[vector[int].iterator, int](self.wage_firm_topo[fi2].begin(), self.wage_firm_topo[fi2].end(), hh1):
             return 0
          # swap     
          self._swap_wage_firm(fi1,idx1,fi2,idx2)
          #save for rollback
          self.fi1_old = fi1
          self.fi2_old = fi2
          self.idx1_old = idx1
          self.idx2_old = idx2   
          return 1
       else: return 0 
		    
    cdef _swap_cons_hh_degree(self):
       """
       swap buy-sell link between hh and firms, preserving degree on hh side.
       Weight is preserved on hh side but not on firm side.
       """
       cdef int hh1,hh2,fi1,fi2,len1,len2
       cdef int idx1,idx2 
       # sample
       #hh1=np.random.randint(self.nh)
       #hh2=np.random.randint(self.nh-1)
       hh1=int(drand48()*self.nh) 
       hh2=int(drand48()*(self.nh-1)) 
       if hh2>=hh1: hh2+= 1 
       #
       len1 = self.cons_hh_topo[hh1].size() #__len__()
       len2 = self.cons_hh_topo[hh2].size() #__len__()
       if len1>0 and len2>0:
		  # random indices 
          #idx1 = np.random.randint(len1)        			  
          #idx2 = np.random.randint(len2)  
          idx1 = int(drand48()*len1)
          idx2 = int(drand48()*len2)
          fi1=self.cons_hh_topo[hh1][idx1]
          fi2=self.cons_hh_topo[hh2][idx2]
          # check it's not the same firm
          if fi1==fi2:
             return 0			      
          # check firm is not already in list 
          if self.cons_hh_topo[hh1].end()!=std_find[vector[int].iterator, int](self.cons_hh_topo[hh1].begin(), self.cons_hh_topo[hh1].end(), fi2):
             return 0
          if self.cons_hh_topo[hh2].end()!=std_find[vector[int].iterator, int](self.cons_hh_topo[hh2].begin(), self.cons_hh_topo[hh2].end(), fi1):
             return 0
          # swap     
          self._swap_cons_hh(hh1,idx1,hh2,idx2)
          #self._swap(self.cons_hh_topo, self.cons_hh_weight,  \
          #           self.cons_firm_topo, self.cons_firm_weight,   \
          #           hh1,idx1,hh2,idx2)
    
          #save for rollback
          self.hh1_old = hh1
          self.hh2_old = hh2
          self.idx1_old = idx1
          self.idx2_old = idx2
          return 1
       else: return 0                     
    # ------------   	
    cpdef get_energy(self):
       """
       keep only positive terms (their sum equals the sum of negative 
       terms, by construction)
       """
       self._balance_hh()
       self._balance_hh()
       cdef double x,s=0.
       cdef int i
       for i in range(self.nf):
          x=  self.balance_firm[i]
          if x>0:  s=s+x
       for i in range(self.nh):
          x=  self.balance_hh[i]
          if x>0:  s=s+x
       return s          
    cpdef copy_state(self,f):
       """
       lil(row) avec cons_firm_weight et wage_firm_weight
       """		
       f(self.cons_firm_topo,self.cons_firm_weight,
         self.wage_firm_topo,self.wage_firm_weight,
         self.get_energy() )	
    cpdef	rollback(self,int type_of_move):
       """
       """  
       if(type_of_move==1):  
          self._swap_cons_hh(self.hh1_old,self.idx1_old,
                                    self.hh2_old,self.idx2_old)
       elif(type_of_move==2):          
          self._swap_wage_firm(self.fi1_old,self.idx1_old,
                                    self.fi2_old,self.idx2_old)                                  
       else:
          raise ValueError('unknown move_type')       		
    def update_energy(self, int dum):
       pass	      
    cpdef propose_move(self, int type_of_move):		
       """
       input:
       -------
       type_of_move:int
       0:not specified 
       1:_swap_cons_hh_degree 
       2:_swap_wage_firm_degree
       output:
       ------
       move_done: int
       0: no move done, 1:_swap_cons_hh_degree 2:
       """
       cdef double r
       if(type_of_move==0):
         r=drand48()
         if r<0.5: type_of_move = 1
         else: type_of_move = 2
       if(type_of_move==1):
          return 1 * self._swap_cons_hh_degree()
       elif(type_of_move==2):
          return 2 * self._swap_wage_firm_degree()   
       else:
          raise ValueError('unknown move_type')    
       #if(type_of_move==1): move_done=self._propose_move_degree()
       #elif(type_of_move==2): move_done=self._propose_move_weight()		
         
    def _print_state(self):
       print "-------"   
       print "self.cons_hh_topo=",self.cons_hh_topo	
       print "self.cons_hh_weight=",self.cons_hh_weight	   	 
       print "self.cons_firm_topo=",self.cons_firm_topo
       print "self.cons_firm_weight",self.cons_firm_weight
       print "-------" 
       print "self.wage_hh_topo=",self.wage_hh_topo
       print "self.wage_hh_weight=",self.wage_hh_weight
       print "self.wage_firm_topo=",self.wage_firm_topo
       print "self.wage_firm_weight=",self.wage_firm_weight
       print "-------" 
    def get_linear_system(self):
       """
       get the system of equations that the state matrices A,C
       must respect.
       
       lil_matrix: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.lil_matrix.html#scipy.sparse.lil_matrix

       F,g
       """   
       cdef int i,val_i	
       # convert adjacency lists to sparse matrices A,C
       A=scipy.sparse.lil_matrix((self.nf,self.nh))
       C=scipy.sparse.lil_matrix((self.nf,self.nh))
       for i,val_i in enumerate(self.cons_firm_topo):
          _A.rows[i]=val_i
          _A.data[i]=1 #ONES (size(val))
       for i,val_i in enumerate(self.wage_firm_topo):
          _C.rows[i]=val_i
          _C.data[i]=1 # ONES(size(val))
       # compute F,g
        		
"""def _swap(self, vector[vector[int]] l1_topo, vector[vector[double]] l1_weight , \
            vector[vector[int]] l2_topo, vector[vector[double]] l2_weight , \
            int hh1, int idx1,int hh2, int idx2):                              
       
       #swap connections 
       
       cdef int fi1,fi2, len1, len2,dum
       cdef double w
       cdef double w1,w2
       # exchange indices in l1_topo 
       fi1=self.l1_topo[hh1][idx1]
       self.l1_topo[hh1][idx1] = self.l1_topo[hh2][idx2]
       self.l1_topo[hh2][idx2] = fi1
       fi2=self.l1_topo[hh1][idx1]
       # exchange indices in l1_weight 
       w1 = self.l1_weight[hh1][idx1]
       self.l1_weight[hh1][idx1] = self.l1_weight[hh2][idx2]
       self.l1_weight[hh2][idx2] = w1
       w2=self.l1_weight[hh1][idx1]
       # exchange: in conso_firm_topo[fi1]/weight: remove hh1,add hh2
       #           in conso_firm_topo[fi2]/weight: remove hh2,add hh1
       # remove weight
       #idx1 = self.l2_topo[fi1].index(hh1)
       #idx2 = self.l2_topo[fi2].index(hh2)       
       cdef vector[int].iterator it_begin = self.l2_topo[fi1].begin()
       cdef vector[int].iterator it_end = self.l2_topo[fi1].end()
       idx1 = std_distance[vector[int].iterator](it_begin , \
               std_find[vector[int].iterator, int](it_begin, it_end, hh1) )                                  					
       cdef vector[int].iterator it_begin = self.l2_topo[fi2].begin()
       cdef vector[int].iterator it_end = self.l2_topo[fi2].end()               
       idx2 = std_distance[vector[int].iterator](it_begin , \
               std_find[vector[int].iterator, int](it_begin, it_end, hh2) ) 
       #print "idx2=",idx2, " self.l2_weight[fi2]=",self.l2_weight[fi2]
       self.l2_topo[fi1][idx1]=hh2
       self.l2_topo[fi2][idx2]=hh1
       w=self.l2_weight[fi1][idx1]
       self.l2_weight[fi1][idx1] = self.l2_weight[fi2][idx2]
       self.l2_weight[fi2][idx2]=w  
    def _sum_list(self,vector[double] l):
       
       #sums the values in a list
       
       cdef double value,s = 0.
       for value in l:
            s += value
       return s"""


cdef class AB_SFC_macro1(object):
    """
    
    TODO: use stdlib list, vector : https://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html#standard-library
          OR lilmatrix
    """
    cdef int nf,nh
    cdef list cons_hh_topo
    cdef list cons_hh_weight
    cdef list cons_firm_topo
    cdef list cons_firm_weight
    cdef list wage_hh_topo
    cdef list wage_hh_weight
    cdef list wage_firm_topo
    cdef list wage_firm_weight
    cdef double [:] balance_hh
    cdef double [:] balance_firm
    cdef int hh1_old, hh2_old,idx1_old,idx2_old # for rollback
    def __init__(self,int nf,int nh, A, C):
       """
       input:
       ------
       A: sparse array, shape = (nf,nh)
       consumption paid to firms by hh. Values in IR+
       
       C: sparse array, shape = (nf,nh)
       wage paid to hh by firms. Values in IR+
       """		
       self.nf = nf
       self.nh = nh
       self.cons_hh_topo= []
       self.cons_hh_weight= []
       self.cons_firm_topo= []
       self.cons_firm_weight= []       
       self.wage_hh_topo= []
       self.wage_hh_weight= []
       self.wage_firm_topo= []
       self.wage_firm_weight= []
       self.balance_hh=np.zeros(nh,dtype=float)
       self.balance_firm=np.zeros(nf,dtype=float)
       if not scipy.sparse.isspmatrix_csr(A):
          raise TypeError('A must be csr matrix') 
       if not scipy.sparse.isspmatrix_csr(C):
          raise TypeError('C must be csr matrix') 	  
       if not A.shape==(nf,nh):
          raise ValueError('A:invalid size')
       if not C.shape==(nf,nh):
          raise ValueError('C:invalid size')
       self._init(A,C) 
    def _sum_list(self,list l):
       """
       sums the values in a list
       """
       cdef double value,s = 0.
       for value in l:
            s += value
       return s    		         
    def _balance_hh(self):		
       """
       compute the balance of households
       """
       cdef int i
       for i in range(self.nh): 
          self.balance_hh[i] = -self._sum_list(self.cons_hh_weight[i]) \
                               + self._sum_list(self.wage_hh_weight[i])           		   
    def _balance_firm(self): 
       """
       compute the balance of firms
       """
       cdef int i
       for i in range(self.nf): 
          self.balance_firm[i] =  self._sum_list(self.cons_firm_weight[i]) \
                                  - self._sum_list(self.wage_firm_weight[i]) 								
    def _swap_cons_hh(self, int hh1, int idx1,int hh2, int idx2):                              
       """
       """
       cdef int fi1,fi2, len1, len2,dum
       cdef double w
       cdef double w1,w2
       # exchange indices in cons_hh_topo 
       fi1=self.cons_hh_topo[hh1][idx1]
       self.cons_hh_topo[hh1][idx1] = self.cons_hh_topo[hh2][idx2]
       self.cons_hh_topo[hh2][idx2] = fi1
       fi2=self.cons_hh_topo[hh1][idx1]
       # exchange indices in cons_hh_weight 
       w1 = self.cons_hh_weight[hh1][idx1]
       self.cons_hh_weight[hh1][idx1] = self.cons_hh_weight[hh2][idx2]
       self.cons_hh_weight[hh2][idx2] = w1
       w2=self.cons_hh_weight[hh1][idx1]
       # exchange: in conso_firm_topo[fi1]/weight: remove hh1,add hh2
       #           in conso_firm_topo[fi2]/weight: remove hh2,add hh1
       # remove weight
       idx1 = self.cons_firm_topo[fi1].index(hh1)
       idx2 = self.cons_firm_topo[fi2].index(hh2)
       #print "idx2=",idx2, " self.cons_firm_weight[fi2]=",self.cons_firm_weight[fi2]
       self.cons_firm_topo[fi1][idx1]=hh2
       self.cons_firm_topo[fi2][idx2]=hh1
       dum=self.cons_firm_weight[fi1][idx1]
       self.cons_firm_weight[fi1][idx1] = self.cons_firm_weight[fi2][idx2]
       self.cons_firm_weight[fi2][idx2]=dum
    def _swap_cons_hh_degree(self):
       """
       swap buy-sell link between hh and firms, preserving degree on hh side.
       Weight is preserved on hh side but not on firm side.
       """
       cdef int hh1,hh2,fi1,fi2,dum#, len1, len2,
       #cdef double w
       cdef int idx1,idx2 
       #cdef double w1,w2
       # sample
       hh1=np.random.randint(self.nh)
       hh2=np.random.randint(self.nh-1)
       if hh2>=hh1: hh2+= 1 
       #
       len1 = self.cons_hh_topo[hh1].__len__()
       len2 = self.cons_hh_topo[hh2].__len__()
       if len1>0 and len2>0:
		  # random indices 
          idx1 = np.random.randint(len1)        			  
          idx2 = np.random.randint(len2)  

          fi1=self.cons_hh_topo[hh1][idx1]
          fi2=self.cons_hh_topo[hh2][idx2]
          # check it's not the same firm
          if fi1==fi2:
             return 0			      
          # check firm is not already in list   
          dum=self.cons_hh_topo[hh1].count(fi2)  # should be l.index()
          if dum>0: return 0
          dum=self.cons_hh_topo[hh2].count(fi1)
          if dum>0: return 0
          # swap     
          self._swap_cons_hh(hh1,idx1,hh2,idx2)    
          #save for rollback
          self.hh1_old = hh1
          self.hh2_old = hh2
          self.idx1_old = idx1
          self.idx2_old = idx2
          return 1
       else: return 0   		
       
    def _shuffle_cons_hh_weight(self):
       """
       select 1 hh and shuffle weight across consumption links.
       total consumption weight is preserved on hh side but not on firm side.
       """ 
       pass  

    def _swap_wage_firm_degree(self):
       """
       swap wage link between hh and firms, preserving degree on firm side.
       Weight is preserved on firm side but not on hh side.
       """ 
       pass      

       
    def _shuffle_wage_firm_weight(self):
       """
       select 1 firm and shuffle weight across wage links
       total wage weight is preserved on firm side but not on hh side.
       """ 
       pass      
    def _init(self,A,C):   
       """
       input:
       ------
       A: sparse csr array, shape = (nf,nh)
       consumption paid to firms by hh
       
       C: sparse csr array, shape = (nf,nh)
       wage paid to hh by firms
       """	
       cdef int i	
       # fill cons_hh_topo and cons_hh_weight
       for i in range(0,self.nh):		   
          self.cons_hh_topo.append( A.getcol(i).tocsc().indices.tolist() )
          self.cons_hh_weight.append( A.getcol(i).tocsc().data.tolist()  )
       # fill cons_firm_topo and cons_firm_weight 
       for i in range(0,self.nf):       
          self.cons_firm_topo.append(A.getrow(i).indices.tolist()  )
          self.cons_firm_weight.append(A.getrow(i).data.tolist()  )
       # fill wage_hh_topo and wage_hh_weight
       for i in range(0,self.nh):		   
          self.wage_hh_topo.append( C.getcol(i).tocsc().indices.tolist() )
          self.wage_hh_weight.append( C.getcol(i).tocsc().data.tolist()  )			
       # fill wage_firm_topo and wage_hh_weight   
       for i in range(0,self.nf):       
          self.wage_firm_topo.append(C.getrow(i).indices.tolist()  )
          self.wage_firm_weight.append(C.getrow(i).data.tolist()  )
          
	# ------------   	
    def get_energy(self):
       self._balance_hh()
       self._balance_hh()
       s=0.
       cdef int i
       for i in range(self.nf):
          s+=np.abs(self.balance_firm[i] )
       for i in range(self.nh):
          s+=np.abs(self.balance_hh[i] )
       return s          
    def copy_state(self,f):
       """
       lil(row) avec cons_firm_weight et wage_firm_weight
       """		
       f(self.cons_firm_topo,self.cons_firm_weight,
         self.wage_firm_topo,self.wage_firm_weight )		   
    def	rollback(self,int type_of_move):
       """
       """  
       if(type_of_move==1):  
          self._swap_cons_hh(self.hh1_old,self.idx1_old,
                                    self.hh2_old,self.idx2_old)    
       else:
          raise ValueError('unknown move_type')       		
    def update_energy(self, int dum):
       pass		
    def propose_move(self, int type_of_move):		
       """
       input:
       -------
       type_of_move:int
       1:_swap_cons_hh_degree 
       output:
       ------
       move_done: int
       0: no move done, 1:_swap_cons_hh_degree
       """
       if(type_of_move==1):
          return 1 * self._swap_cons_hh_degree()
       else:
          raise ValueError('unknown move_type')    
       #if(type_of_move==1): move_done=self._propose_move_degree()
       #elif(type_of_move==2): move_done=self._propose_move_weight()		

    def _print_state(self):
       print "-------"   
       print "self.cons_hh_topo=",self.cons_hh_topo	
       print "self.cons_hh_weight=",self.cons_hh_weight	   	 
       print "self.cons_firm_topo=",self.cons_firm_topo
       print "self.cons_firm_weight",self.cons_firm_weight
       print "-------" 
       print "self.wage_hh_topo=",self.wage_hh_topo
       print "self.wage_hh_weight=",self.wage_hh_weight
       print "self.wage_firm_topo=",self.wage_firm_topo
       print "self.wage_firm_weight=",self.wage_firm_weight
       print "-------" 
       print [self.balance_hh[i] for i in range(self.nh)]
       print [self.balance_firm[i] for i in range(self.nf)]
    def get_linear_system(self):
       """
       get the system of equations that the state matrices A,C
		must respect.
		
		F,g
		"""
       cdef int i
       e = scipy.sparse.eye(self.nh,format ='csr')	
       F_h = scipy.sparse.eye(self.nh,format ='csr')	
       # budget constraint: hh
       for i in range(self.nf-1): 
         F_h=scipy.sparse.hstack([F_h,e])
       F_h = scipy.sparse.hstack([F_h,-F_h])   
       # budget constraint: firms
       e = scipy.sparse.eye(self.nf,format ='csr')
       o = scipy.sparse.csr_matrix(np.ones((1,self.nh)))	
       F_f = scipy.sparse.kron(e, o)
       F_f = scipy.sparse.hstack([F_f,-F_f]) 
       # total currency
       F_last = scipy.sparse.csr_matrix((1,2*self.nf*self.nh))
       F_last[0,0:self.nf*self.nh] = 1.
       #F_last[self.nf*self.nh:2*self.nf*self.nh] = 0.
       F = scipy.sparse.vstack([F_h,F_f,F_last])
       # rhs
       g = np.zeros(self.nh+self.nf+1)	   
       g[self.nh+self.nf]=1.
       return F,g   
           
cdef class GraphCSR(object):
    """
    !!! PROBLEM: degree-preserving moves need a modification of x0 and A. 
    !!! THIS IS NOT SUPPORTED SO FAR
    
    TODO: full CSR, not just A but also x and b
    
    "The CSR format is specially suitable for fast matrix vector products." https://docs.scipy.org/doc/scipy/reference/sparse.html
    
    https://stackoverflow.com/questions/25295159/how-to-properly-pass-a-scipy-sparse-csr-matrix-to-a-cython-function
    http://www.mathcs.emory.edu/~cheung/Courses/561/Syllabus/3-C/sparse.html

 
    NB: this will not work: (see https://stackoverflow.com/questions/24114026/using-numpy-array-in-cython#24166170)
    cdef np.ndarray[cINT32, ndim=1]  A_indices, A_indptr
    """	
    cdef int [:]  A_indices_csr, A_indptr_csr,A_indices_csc, A_indptr_csc
    cdef double [:] A_data_csr,A_data_csc
    cdef int [:]  x0_indices, x0_indptr#,b_indices
    cdef double [:] x0_data #,b_data
    cdef double [:] b
    cdef int nr_A,nc_A,nr_x0,nc_x0
    cdef int i_csr_old,j_csr_old # for rollback
    cdef double x0_data_i_old, x0_data_j_old # for rollback    
    cdef double [:] delta # Ax-b
    cdef double [:] delta_old
    cdef int i_lin_new,i_lin_old,j_lin_new,j_lin_old # for fast energy computation
    cdef int init_done
    #cdef char [:] energy_str
    def __init__(self, np.ndarray[np.double_t] A_data_csr, np.ndarray[int] A_indices_csr, np.ndarray[int] A_indptr_csr,
                       np.ndarray[np.double_t] A_data_csc, np.ndarray[int] A_indices_csc, np.ndarray[int] A_indptr_csc,     
					   int nr_A, int nc_A,	
					   #np.ndarray[np.double_t] x0,
					   np.ndarray[np.double_t] x0_data,
					   np.ndarray[int] x0_indices,
					   np.ndarray[int] x0_indptr,
					   int nr_x0, int nc_x0,
					   np.ndarray[np.double_t] b,
					   #np.ndarray[np.double_t] b_data, 
					   #np.ndarray[int] b_indices,
						):
        self.nr_A=nr_A								
        self.nc_A=nc_A
        self.nr_x0=nr_x0						
        self.nc_x0=nc_x0
        self.A_data_csr=A_data_csr				   
        self.A_indices_csr=A_indices_csr
        self.A_indptr_csr=A_indptr_csr
        self.A_data_csc=A_data_csc				  
        self.A_indices_csc=A_indices_csc
        self.A_indptr_csc=A_indptr_csc
        #self.x0=x0
        self.b=b
        self.x0_data=x0_data				   
        self.x0_indices=x0_indices
        self.x0_indptr=x0_indptr
        #self.b_data=b_data				   
        #self.b_indices=b_indices  
        #self.energy_str=energy_str
        self.delta = np.zeros(nr_A,dtype=np.float)
        self.delta_old = np.zeros(nr_A,dtype=np.float)
    """    self.init_done=0
    def init(self):
		# compute delta= Ax0-b
        self._get_delta()                                 
        self.init_done =1    """
    def _print_state(self):
        print "i_csr_old,j_csr_old=",self.i_csr_old,self.j_csr_old
        print "self.x0_data_i_old, self.x0_data_j_old=",self.x0_data_i_old, self.x0_data_j_old 
        print "self.delta=",self.delta[0], self.delta[1],self.delta[2],self.delta[3]
        print "energy=",self.get_energy()
    def get_energy(self):
        return self._get_delta_sum()		    
    def update_energy(self,move_done):
        """
        compute sum((Ax-b)**2)
        """
        cdef int i
        # save old delta
        for i in range(self.nr_A):
            self.delta_old[i] =self.delta[i]
        # dot product   
        A_csr = scipy.sparse.csr_matrix((self.A_data_csr, self.A_indices_csr, self.A_indptr_csr),
                        shape=(self.nr_A,self.nc_A ))
        x0 = scipy.sparse.csr_matrix((self.x0_data, self.x0_indices, self.x0_indptr),
                        shape=(self.nr_x0,self.nc_x0 ))    
                                    
        self.delta = A_csr.dot( np.reshape(np.array(x0.todense()),
                        self.nr_x0*self.nc_x0))	
        # delta = delta - b                
        for i in range(self.nr_A):
            self.delta[i] = self.delta[i]  - self.b[i]              					
    def _get_energy(self,move_done): 
        """
        DEPRECATED compute sum((Ax-b)**2)
        """
        cdef np.ndarray[np.double_t] result=np.zeros(self.nr_A) 
        cdef int i,k,m,n
        if(move_done==1):
           # get A*delta_x
           self.add_dot_A_xei_CSC(result, self.i_lin_new, 1)
           self.add_dot_A_xei_CSC(result,self.j_lin_new, 1)
           self.add_dot_A_xei_CSC(result, self.i_lin_old, -1)
           self.add_dot_A_xei_CSC(result,self.j_lin_old, -1)            			  
        elif(move_done==2):
           pass
        # compute delta = delta0 + A*delta_x        
        for i in range(self.nr_A):
            self.delta[i] = self.delta[i]+result[i]					  						  
        return self._get_delta_norm()
    def _get_delta_norm(self): 
        """
        compute sum(delta[i]**2)
        """
        cdef int i
        cdef double r=0.,x
        for i in range(self.nr_A):
            x =	self.delta[i]
            r = r + x*x
        return r 
    def _get_delta_sum(self): 
        """
        compute sum(delta[i]**2)
        """
        cdef int i
        cdef double r=0.
        for i in range(self.nr_A):
            r = r + self.delta[i]
        return r            
    def set_delta(self, np.ndarray[np.double_t] delta):     
        cdef int i        
        for i in range(self.nr_A):
            self.delta[i] = delta[i]
    def propose_move(self, int type_of_move):		
        """
        input:
        -------
        type_of_move:int
        0: both with equal probablity, 1:degree-preserving only, 2: weight-preserving only		

        output:
        ------
        move_done: int
        0: no move done, 1:degree-preserving move, 2: weight-preserving move
		"""
        cdef int move_done
        if(type_of_move==0):
            type_of_move = np.random.randint(2) +1
        if(type_of_move==1): move_done=self._propose_move_degree()
        elif(type_of_move==2): move_done=self._propose_move_weight()		
        return move_done
		
    def _propose_move_weight(self):		
        """
        redistribute weight between two edges connected to the same node
        """
        cdef int i_csr,j_csr,k, move_done=0 
        cdef double r,s
        k = np.random.randint(self.nr_x0)
        if( self.x0_indptr[k+1]- self.x0_indptr[k]>1):
           # tirage sans remise
           i_csr = 	np.random.randint(self.x0_indptr[k], self.x0_indptr[k+1])
           j_csr = 	np.random.randint(self.x0_indptr[k], self.x0_indptr[k+1]-1)
           if j_csr >= i_csr: j_csr = j_csr+1           
           # record for rollback
           self.i_csr_old = i_csr
           self.j_csr_old = j_csr
           self.x0_data_i_old = self.x0_data[i_csr]
           self.x0_data_j_old = self.x0_data[j_csr]
           # redistribute
           r = np.random.rand()
           s = self.x0_data[i_csr] + self.x0_data[j_csr]
           self.x0_data[i_csr] = r* s
           self.x0_data[j_csr] = (1.-r)*s
           move_done=2
        return move_done
        
    def _propose_move_degree(self):
        """
        degree preserving move
        """
        cdef int nz_x0, i_csr,j_csr,k,row_i,row_j
        nz_x0 = np.shape(self.x0_data)[0] 
        cdef move_done=0  
        # get two distinct random numbers in [0, nz_x0[
        i_csr = np.random.randint(nz_x0)
        j_csr = np.random.randint(nz_x0-1)
        if j_csr>=i_csr: j_csr+= 1
        # check they concern different rows
        row_i = self.csr2row(i_csr,self.nr_x0+1)
        row_j = self.csr2row(j_csr,self.nr_x0+1)        
        # exchange    
        if row_i != row_j:
			 # record old linear index for energy computation
             self.i_lin_old= row_i * self.nc_x0 + self.x0_indices[i_csr]
             self.j_lin_old= row_j * self.nc_x0 + self.x0_indices[j_csr]
             #print self.i_lin_old, row_i , self.nc_A ,i_csr, self.x0_indices[i_csr]
			 # swap column indices in x0
             self._swap_edges(i_csr, j_csr)
             # record csr index for rollback (if necessary)
             self.i_csr_old = i_csr
             self.j_csr_old = j_csr
             # record new linear index for energy computation
             self.i_lin_new = row_i * self.nc_x0 + self.x0_indices[i_csr]
             self.j_lin_new = row_j * self.nc_x0 + self.x0_indices[j_csr]          
             move_done=1
        return move_done     
    def _swap_edges(self,int i_csr, int j_csr):
        """
        swap edges in a degree-preserving way.
        swap self.x0_indices[row_i] and 
        """		
        cdef int buf=self.x0_indices[i_csr]     			    
        self.x0_indices[i_csr] = self.x0_indices[j_csr] 
        self.x0_indices[j_csr] = buf  
    def _rollback_weight(self,int i_csr, int j_csr):                 
        """
        redistribute weights between two nodes in a random way
        """
        self.x0_data[i_csr] = self.x0_data_i_old
        self.x0_data[j_csr] = self.x0_data_j_old
    def rollback(self, type_of_move):          
        """
        revert move
        
        input:
		-------
		type_of_move:int
        1:degree-preserving, 2: weight-preserving
        """	
        cdef int i
        if(type_of_move==1):
           self._swap_edges(self.i_csr_old, self.j_csr_old)
        elif (type_of_move==2):   
           self._rollback_weight(self.i_csr_old, self.j_csr_old) 
        # bring back delta_old   
        for i in range(self.nr_A):
            self.delta[i] = self.delta_old[i]    
    def copy_state(self,f):
        """
        use the supplied function f to copy the current state x0
        """
        #f(csr_matrix((self.x0_data, self.x0_indices, self.x0_indptr), shape=(self.nr_x0,self.nc_x0 )) , self.get_energy(0), i )
        f(scipy.sparse.csr_matrix((self.x0_data, self.x0_indices, self.x0_indptr), shape=(self.nr_x0,self.nc_x0 )) , self.get_energy() )
    def csr2row(self, int i, int nnz):     
        """
        compute the row corresponding to the linear csr index
        input:
        -----
        i: int
        linear csr index, must be <nnz

        indptr: int array
        indptr in csr array

        nnz: int
        number of non-zero terms in matrix		
        """   
        cdef int k
        #if i>=nnz: return -1
        #if i<0: return -1
        for k in range(1,nnz+1):
           if(i<self.x0_indptr[k] ):             
               return k-1#break
    cdef add_dot_A_xei_CSC(self,np.ndarray[np.double_t] result,
                  int i, double x):
       """
       add  A * (x*ei) to result, where ei is a basis vector (e.g. (0..1..0))
       
       howto get A[i,:] with CSC matrix:
       https://www.scipy-lectures.org/advanced/scipy_sparse/csc_matrix.html
          "nonzero values of the i-th column are data[indptr[i]:indptr[i+1]] with row indices indices[indptr[i]:indptr[i+1]]"
       
       input:
       -------   
       A_data: np.ndarray, dtype=double
       data array of A in CSC representation
       
       A_indices: np.ndarray, dtype=int
       indices array of A in CSC representation
       
       A_indptr: np.ndarray, dtype=int
       indptr array of A in CSC representation
        
       result: np.ndarray, dtype=double
       null array where the result is recorded
    
       i: int
       index of basis vector ei, i.e. of the column of A
    
       x: double
  
       """
       cdef int k,l
       cdef double d
       for k in range(self.A_indptr_csc[i],self.A_indptr_csc[i+1]): 
           l = self.A_indices_csc[k] 		
           d = self.A_data_csc[k] 		
           #result[l] = np.double(d)*delta_x_i
           result[l] = result[l] + d*x

"""
    def _get_delta(self):         
        #compute Ax-b
        #cdef np.ndarray[np.double_t] result=np.zeros(self.nr_A) 
        cdef int i,k,m,n
        # compute Ax0
        for i in range(self.nr_A):    
            for k in range(self.A_indptr_csr[i], self.A_indptr_csr[i+1]):                
                #https://docs.scipy.org/doc/numpy/reference/generated/numpy.unravel_index.html#numpy.unravel_index
                (m,n) = np.unravel_index(self.A_indices_csr[k], (self.nr_x0,self.nc_x0)) 
                self.delta[i] = self.delta[i] + self.A_data_csr[k]*self._get_x0(m,n);#self.x0[self.A_indices[k]];       
        # compute Ax0-b
        for i in range(self.nr_A):
            self.delta[i] = self.delta[i]-self.b[i]		
    def _get_x0(self, int i, int j):
        #return x0[i,j]		
        cdef double r
        cdef int k
        # empty row
        if self.x0_indptr[i]==self.x0_indptr[i+1]:
            return 0.
        else:    
            # look for j in x0_indptr[i]:self.x0_indptr[i+1]	
            for k in range(self.x0_indptr[i],self.x0_indptr[i+1]):
                if j>self.x0_indices[k]: 
                   return 0.
                elif j==self.x0_indices[k]:
                   #https://www.scipy-lectures.org/advanced/scipy_sparse/csr_matrix.html
                   return self.x0_data[self.x0_indptr[i]+k]
                else: # j>self.x0_indices[k]:      
                   pass
            return 0.                   
"""            

#################################################   
# tools
cpdef add_dot_A_xei_CSC(np.ndarray[np.double_t] A_data, np.ndarray[int] A_indices, 
                  np.ndarray[int] A_indptr,	np.ndarray[np.double_t] result,
                  int i, double x):
    """
    add  A * (x*ei) to result, where ei is a basis vector (e.g. (0..1..0))
    
    howto get A[i,:] with CSC matrix:
    https://www.scipy-lectures.org/advanced/scipy_sparse/csc_matrix.html
       "nonzero values of the i-th column are data[indptr[i]:indptr[i+1]] with row indices indices[indptr[i]:indptr[i+1]]"
    
    input:
    -------   
    A_data: np.ndarray, dtype=double
    data array of A in CSC representation
    
    A_indices: np.ndarray, dtype=int
    indices array of A in CSC representation
    
    A_indptr: np.ndarray, dtype=int
    indptr array of A in CSC representation
        
    result: np.ndarray, dtype=double
    null array where the result is recorded
    
    i: int
    index of basis vector ei, i.e. of the column of A
    
    x: double
  
    """
    cdef int k,l
    cdef double d
    for k in range(A_indptr[i],A_indptr[i+1]): 
        l = A_indices[k] 		
        d = A_data[k] 		
        #result[l] = np.double(d)*delta_x_i
        result[l] = result[l] + d*x
    
def csr2row(self, int i, np.ndarray[int] indptr,int nnz):     
        """
        compute the row corresponding to the linear csr index
        input:
        -----
        i: int
        linear csr index, must be <nnz

        indptr: int array
        indptr in csr array

        nnz: int
        number of non-zero terms in matrix		
        """   
        cdef int k
        #if i>=nnz: return -1
        #if i<0: return -1
        for k in range(1,nnz+1):
           if(i<indptr[k] ):             
               return k-1#break        
"""
cdef dot_CSR(np.ndarray[np.double_t] A_data, np.ndarray[int] A_indices, np.ndarray[int] A_indptr,
			int nr,
			np.ndarray[np.double_t] x,
			np.ndarray[np.double_t] result	):

    https://stackoverflow.com/questions/25295159/how-to-properly-pass-a-scipy-sparse-csr-matrix-to-a-cython-function
    http://www.mathcs.emory.edu/~cheung/Courses/561/Syllabus/3-C/sparse.html
	for (i = 0; i < N; i = i + 1)
   {  
      for (k = A_indptr[i]; k < A_indptr[i+1]; k = k + 1)
      {  
         result[i] = result[i] + A_data[k]*x[A_indices[k]];
      }  
   }  
    cdef int i,k
    for i in range(nr):    
		for k in range(A_indptr[i], k < A_indptr[i+1]):
			result[i] = result[i] + A_data[k]*x[A_indices[k]];

cdef residual_linsys(A,x,b):
    cdef np.ndarray[cDOUBLE, ndim=1] Ax
    cdef int nr
    cdef int nc
    nr= np.shape(A)[0]
    nc= np.shape(A)[1]
	#ctypedef np.int32_t cINT32
    Ax= A.dot(x).toarray().flatten().astype(np.float64)
    res = Ax-b.toarray()
    return np.sum(res**2)
    """
#################################################   
# Histogram    
    
cdef class Histogram:
    """
    copied from https://github.com/SamplingConstrainedNetworks/code/blob/master/source/histogram.h
    by JC Leitao and EG Altmann
    
    TODO: histogram for int or double, template-style 
          see http://docs.cython.org/en/latest/src/userguide/fusedtypes.html
    """
    cdef double _lowerBound
    cdef double _upperBound
    cdef int _bins;  # number of bins of the histogram
    cdef int _count; # number of measured samples
    cdef int [:] _histogram
    cdef double [:] _entropy
    
    def __init__(self, double lowerBound, double upperBound, int bins):
        self._lowerBound=lowerBound
        self._upperBound=upperBound;
        self._bins=bins;  # number of bins of the histogram
        self._count=0; # number of measured samples
        self._histogram=np.zeros(bins+1,dtype=np.int32)
        self._entropy=np.zeros(bins+1)
        # TODO check lb<ub
    def reset(self):	
        for i in range(self._bins+1):    
           self._histogram[i]=0
        self._count=0	    
    def bin(self,double value): # bin of current energy val
        if (value <= self._lowerBound):
            return 0;
        if (value >= self._upperBound):
            return self._bins
        cdef int b = int((value - self._lowerBound)*(1.0*self._bins)/(self._upperBound - self._lowerBound)        )
        #assert(b < self._bins)
        return b
    def entropy(self,double value): 
        cdef int b = self.bin(value)
        return self._entropy[b]		    
    def bins(self): # max bin
        return self._bins
    def add(self,double value ,double f): # update histogram with new value
        cdef int b = self.bin(value)
        self._histogram[b] +=1
        self._entropy[b] +=f
        self._count+=1
    def count(self):
        return self._count
    def print_(self):
        print "histogram="
        for i in range(self.bins()):    
            print(self._histogram[0],self._histogram[1],self._histogram[2],self._histogram[3] )
        print '\n'    
        #print(self._histogram)
        #print "entropy=", self._entropy
    def get_histogram(self,np.ndarray[int] H):
        cdef int i
        for i in range(self._bins+1): 
            H[i]=self._histogram[i]
    def get_entropy(self,np.ndarray[np.double_t] S): 
        cdef int i
        for i in range(self._bins+1): 
            S[i]=self._entropy[i]
    def get_upperBound(self):
        return self._upperBound    
    def get_lowerBound(self):
        return self._lowerBound
    def isflat(self,double flatness):
        """
        adapted from cf wangLand.py
        """
        cdef int i
        cdef double aH=0.;     # mean Histogram
        cdef double mH=1e100; # minimum of the Histogram
        for i in range(self._bins+1):    
          aH += self._histogram[i]
          if (self._histogram[i]<mH): mH = self._histogram[i]  
        aH *= 1./(1.*self._bins+1)
        if (mH > aH*flatness): return 1
        else: return 0   
            
#################################################   
# MC/WL steps

cdef mc_step(graph,lnh, nsteps):  
    cdef int move_done=0 
    cdef double e,e_new,r
    cdef int was_accepted=1
    e=graph.get_energy()
    move_done =graph.propose_move(0)
    if(move_done>0):
        graph.graph.update_energy(move_done)
        e_new=graph.get_energy()
        r=min(1., math.exp(lnh.entropy(e_new)- lnh.entropy(e)) ) 
        # if rejected
        if ( np.random.rand() > r):
            graph.rollback(move_done)
            was_accepted = 0
    
cdef wl_step(sys, lnh, double f, int move_type=0):  
    """
    accept if : log(r)< S(e) -S(e_new)
    histogram is updated
    """	      
    cdef int move_done=0	    
    cdef double e,e_new
    e=sys.get_energy()
    #e=graph._get_delta_norm()
    move_done = sys.propose_move(move_type)
    # if rejected
    if(move_done>0):
        sys.update_energy(move_done)
        e_new=sys.get_energy()
        if ( math.log(drand48()) < lnh.entropy(e)- lnh.entropy(e_new) ):
           lnh.add(e_new,f)
        else:    
        #if ( math.log(np.random.rand()) > lnh.entropy(e)- lnh.entropy(e_new) ):
        #if ( np.random.rand() > math.exp(lnh.entropy(e)- lnh.entropy(e_new)) ):
           sys.rollback(move_done)
           lnh.add(e,f)  
            
            

    
cdef wl_sample(graph, lnh, f, int round_trips = 5, int verbose=0):       
    # propose new graph fragment_graph
    # compute x from fragment_graph 
    # round trip control
    cdef int going_up
    cdef double e
    cdef int count = 0
    for round_trip in range(round_trips):
        going_up = 1
        while (True):
            wl_step(graph, lnh, f)
            e = graph.get_energy()	
            #if lnh.bin(e) == lnh.bins() and going_up:  ???????????????
            if lnh.bin(e) == lnh.bins()-1 and going_up:	
                going_up = 0
            elif lnh.bin(e) == 1 and not going_up: break # bin=0!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
            count +=1
            if verbose==2 and count==100:
                print ' e=%3.3f '%(e), lnh.bin(e),'/', lnh.bins(),'lb=' ,lnh.get_lowerBound(), ' ub=' ,lnh.get_upperBound(), ' round_trip=',round_trip, '/',round_trips
                count=0
#cdef wl_sample_copy(graph, lnh, f, round_trips = 5, copy_nmax=-1, copy_lb=-0.1, copy_ub=0.1,copy_func=None):       
cdef wl_sample_copy(graph, lnh, f, round_trips = 5, copy_lb=-0.1, copy_ub=0.1,copy_func=None,int verbose=0):       	
    # propose new graph fragment_graph
    # compute x from fragment_graph 
    # round trip control
    cdef int going_up
    cdef double e
    cdef int count = 0
    #cdef int count_copy = 0, 
    cdef int done_copy
    for round_trip in range(round_trips):
        going_up = 1
        done_copy=0
        while (True):
            wl_step(graph, lnh, f)
            e = graph.get_energy()	
            #if lnh.bin(e) == lnh.bins() and going_up:  ???????????????
            if lnh.bin(e) == lnh.bins()-1 and going_up:	
                going_up = 0
            elif lnh.bin(e) == 1 and not going_up: break # bin=0!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
            count +=1
            # copy             
            if done_copy==0 and e<copy_ub and e>copy_lb:               
               graph.copy_state(copy_func)
               done_copy= 1
            if verbose>1 and count==100:
                print ' e=%3.3f '%(e), lnh.bin(e),'/', lnh.bins(),'lb=' ,lnh.get_lowerBound(), ' ub=' ,lnh.get_upperBound(), ' round_trip=',round_trip, '/',round_trips
                count=0
                
   
#def wl_simulate_net(graph, lnh,finit=1., fmin=1.e-4, lowerBound=0.0, upperBound=0.1, bins=50, round_trips=5 ,copy_nmax=-1, copy_lb=-0.1, copy_ub=0.1,copy_func=None,verbose=True):
def wl_simulate_net(graph, lnh,finit=1., fmin=1.e-4, lowerBound=0.0, upperBound=0.1, bins=50, round_trips=5 ,
                    do_copy=False,copy_lb=-0.1, copy_ub=0.1,copy_func=None,int verbose=1):
    """
    """
    cdef double f = finit
    while f >= 0.5 * fmin:
        lnh.reset()
        if verbose>0:
            print("================\nW-L calculation: f = %g" % f);
        if do_copy: #copy_nmax>0:
           #wl_sample_copy(graph, lnh, f,round_trips,copy_nmax, copy_lb, copy_ub,copy_func)  
           wl_sample_copy(graph, lnh, f,round_trips, copy_lb, copy_ub,copy_func,verbose=verbose)  
        else:
           wl_sample(graph, lnh, f,round_trips,verbose=verbose )  
        #py_lnhEV = histogramEV_c_to_py(target, lnhEV)
        #with open(output, 'w') as out:
        #    out.write("# f = %g\n\n" % f)
        #    py_lnhEV.write(out)
        f *= 0.5        
    return lnh 
 
cpdef wl(sys, lnh,int move_type=0, double finit=1., double fmin=0.0001, int niter=1000000 ,
             double flatness=0.9, double copy_lb=-0.1, double copy_ub=0.1,int do_copy=0,copy_func=None,int verbose=1):
    """
    
    input:
    ------
    sys:  class
    system under study
    """
    cdef double e,f = finit
    cdef int i
    for i in range(niter):
        wl_step(sys, lnh, f, move_type)
        if i%100==0:
           if lnh.isflat(flatness):	
              f *= 0.5
              if f<fmin: return lnh,f
              lnh.reset()              
        if do_copy>0 and i%do_copy==0:
           e = sys.get_energy()				
           if  e<copy_ub and e>copy_lb:               
              sys.copy_state(copy_func)           
        if verbose>1 and i%100000==0:
           e = sys.get_energy()
           print 'f=',f,' e=%f '%(e), lnh.bin(e),'/', lnh.bins(),'lb=' ,lnh.get_lowerBound(), ' ub=' ,lnh.get_upperBound(), ' i=',i               
    return lnh , f 
 
def mc_simulate_net(graph, lnh, nsteps, nsamples,lowerBound=0.0, upperBound=0.1, bins=50,):
    """
    """
    marginal = 0
    sample=0

    for sample in range(nsamples):
        mc_step(graph , lnh, nsteps)
        # compute incremental marginal
		
    return marginal, sample 

# -*- coding: utf-8 -*-
import numpy as np  
import scipy.sparse
import scipy.stats

import sys
sys.path.append('../sfc_proba')
import nls_solvers

import wl_mcmc_network as wlmcmc  


sys.path.append('../sfc_proba/cython')
import ficm_cython
import ficm

from scipy.stats import pareto
import matplotlib.pyplot as plt

def get_x0_CM(k_in,k_out,f):
	# topo= configuration model
	# get adjacency matrix 
	aij = 0
	# get system to be solved Fx=g over this topo
	F,g = f(aij)
	# nnls solver 
	nnls = nls_solvers.LbfgsNNLS()
	nnls.fit(F,g)
	return nnls.coef_
	
	
def get_x0_FiCM(x_in,x_out,L):
	# topo= fitness-induced configuration model
	pass

	
def get_dataset(d='base'):
	"""
	create a dataset to test wl sampling
	"""
	p={ 'system':{'nr_x0':0, 'nc_x0':0},
		'hist':{'bins':30, 'ub':None,'lb':15} ,
		'sampler':{'type':'wl','nsteps':100,'fmin':0.05,'finit':1.0,
					'nround_trips':5}
		}
	nr_A,nc_A=0,0
	nr_x0,nc_x0=0,0
	A=[]	
	print 'get_dataset: ',d
	if(d=='base'):
		# x0 is not a solution of Ax0-b, 
		nf = 20
		nr_A,nc_A = 50,nf**2	
		nr_x0,nc_x0 = nf,nf
		A = scipy.sparse.rand(nr_A,nc_A,0.1,format='csr')
		b = np.ones(nr_A)
		x0 = scipy.sparse.rand(nr_x0,nc_x0,0.1,format='csr')
		p['hist']['lb'] = -1.
		p['hist']['ub'] = 1. 
		# check x0>0 !!!!!!!!!
	elif(d=='x0_nls'):
		# x0 is a solution of Ax0-b (=> delta~0), 
		# with non-negative least squares (NNLS, =>x0>0) 
		# Sparsity of x0 is uncontrolled
		nf = 20
		nr_A,nc_A = 50,nf**2	
		nr_x0,nc_x0 = nf,nf
		A = scipy.sparse.rand(nr_A,nc_A,0.1,format='csr')
		b = np.ones(nr_A)
		nnls = nls_solvers.LbfgsNNLS()
		nnls.fit(A,b)
		x0 = np.reshape(nnls.coef_,(nf,nf))
		x0 = scipy.sparse.csr_matrix(x0)
		p['hist']['lb'] = -4.
		p['hist']['ub'] = 4. # TODO: ub = 10*norm(Ax0-b)
	elif(d=='x0_nls_L1'):
		# x0 is a solution of Ax0-b, and x0>0 (=> delta~0)
		# with non-negative least squares (NNLS, =>x0>0) 
		# Sparsity of x0 is controlled with L1 regularization
		nf = 20
		nr_A,nc_A = 50,nf**2	
		alpha= 0.1 # NB: if too sparse, WL sampler fails to remain in [lb,ub]
		nr_x0,nc_x0 = nf,nf
		A = scipy.sparse.rand(nr_A,nc_A,0.1,format='csr')
		b = np.ones(nr_A)
		nnls = nls_solvers.LbfgsNNLS_L1(alpha=alpha)
		nnls.fit(A,b)
		x0 = np.reshape(nnls.coef_,(nf,nf))
		x0 = scipy.sparse.csr_matrix(x0)
		p['hist']['lb'] = -5.
		p['hist']['ub'] = 5.0 # TODO: ub = 10*norm(Ax0-b)
	elif(d=='x0_CM'):
		# assume k_i,k_j the degree distrib
		# sample aij from a CM parametrized by k_i,k_j. 
		# define the system Fx=b, with F depending on aij
		# compute x0
		pass
	elif(d=='x0_FiCM'):				
		# x_i,x_j the fitnesses, L the number of links.
		# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		nr_aij = 100
		nc_aij = 1000
		nr_A = 1*nr_aij # 1 constraints per agent
		nc_A = nr_aij * nc_aij
		b=1.1
		rv = pareto(b)	
		xi= rv.rvs(nr_aij)
		yj= rv.rvs(nc_aij)
		L = int(0.01 * (nr_aij*nc_aij))
		# fit FiCM 
		directed = False
		f=ficm.FiCM(xi,yj,L,directed=directed,bipartite=True)					
		x0=0.5	
		args = (xi,yj,xi.size,yj.size,1*directed,float(L))
		f.make_ficm(x0=x0,func=ficm_cython._func_cython,
				jac=ficm_cython._jac_cython,args=args)				
		pij = f.get_biadjacency_matrix(f._pij)											
		if not f.sol.success:
			warnings.warn('fit didnt reach convergence')
		if not 	f.sol.x[0]>0:
			raise ValueError('f.sol.x[0]<=0')
		# sample aij from a FiCM parametrized by k_i,k_j. 
		aij = f.get_random_biadjacency_matrix()
		# define the system Ax=b, with A depending on aij
		wij = np.random.choice([-1, 1], size=(nr_aij, nc_aij)) * aij
		A = scipy.sparse.csr_matrix((nr_A,nc_A))
		#A = np.zeros((nr_A,nc_A))
		for i in range(nr_A):
			A[i, i*nc_aij :(i+1)*nc_aij] = wij[i,:]
		b = np.ones(nr_A)
		#plot
		#plt.matshow(wij, aspect="auto")
		#plt.matshow(A, aspect="auto")
		# compute x0
		nnls = nls_solvers.LbfgsNNLS()#nnls = nls_solvers.LbfgsNNLS_L1(alpha=alpha)
		nnls.fit(A,b)
		x0 = np.reshape(nnls.coef_,(nr_aij,nc_aij))
		x0 = scipy.sparse.csr_matrix(x0)
		p['hist']['lb'] = -4.
		p['hist']['ub'] = 4. 
		# check
		if np.any(np.bitwise_and(aij==0, x0.toarray()>0)):
			raise ValueError('aij==0 => Wij_ficm=0')
		if np.any(nnls.coef_<0):		
			raise ValueError('x0[i]<0 for some i')
		nr_x0,nc_x0 = nr_aij,nc_aij
	else: raise ValueError('unknown dataset')		
	p['system']['nr_x0']=nr_x0
	p['system']['nc_x0']=nc_x0
	p['system']['sparsity_A']=A.nnz/ float(nr_A*nc_A)
	p['system']['sparsity_x0']=x0.nnz/ float(nr_x0*nc_x0)
	return A,b,x0,p

###########
def get_gCSR_lnh(data='x0_nls_L1'):
	"""
	tool function 
	"""
	#A_csr,b,x0_csr,p = get_dataset('base')
	#A_csr,b,x0_csr,p = get_dataset('x0_nls')
	A_csr,b,x0_csr,p = get_dataset(data)
	nr_A,nc_A = A_csr.shape
	nr_x0,nc_x0 = x0_csr.shape
	# Initialize
	bins=p['hist']['bins']
	A_csc = scipy.sparse.csc_matrix(A_csr)

	gCSR0 = wlmcmc.GraphCSR(A_csr.data, A_csr.indices, A_csr.indptr,
							A_csc.data, A_csc.indices, A_csc.indptr,
							nr_A,nc_A,
							x0_csr.data, x0_csr.indices,x0_csr.indptr,
							nr_x0,nc_x0,						
							b) 
	#delta = A_csr.dot(x0_csr)						
	delta = A_csr.dot( np.reshape(np.array(x0_csr.todense()),nr_x0*nc_x0))					
	gCSR0.set_delta(delta)						
	#gCSR0.init()						    
	lowerBound = p['hist']['lb']
	upperBound = p['hist']['ub']
	if upperBound == None:
		upperBound = gCSR0._get_delta_norm()
	print 'upperBound=',upperBound
	lnh = wlmcmc.Histogram(lowerBound, upperBound, bins)
	return gCSR0,lnh,p

########################
def get_AB_SFC_macro1(dataset, nf=5, nh=20,density=0.1):
	if dataset=='base':
		# not balanced
		A = scipy.sparse.csr_matrix([[1,0,2,0],
									 [0,3,0,0],
									 [4,5,6,7]], dtype=np.float)
		C = scipy.sparse.csr_matrix([[1,0,0,1],
									 [0,1,0,0],
									 [1,0,1,0]], dtype=np.float)
		return A,C
	elif dataset=='balanced':
		# balanced
		o = scipy.sparse.csr_matrix((nf,nh))
		sys=wlmcmc.AB_SFC_macro1(nf,nh,o,o)
		F,g=sys.get_linear_system()	
		# sample consumption matrix 
		A_topo = (1*(np.random.rand(nf*nh)<density))
		A = A_topo * np.random.lognormal(size=nf*nh)
		gg=F.toarray()[:,0:nf*nh].dot(A)
		C,rnorm = scipy.optimize.nnls(F.toarray()[:,nf*nh:2*nf*nh], g-gg)
		C= C.reshape((nf,nh))
		A= A.reshape((nf,nh))
		return scipy.sparse.csr_matrix(A),scipy.sparse.csr_matrix(C)
	elif dataset=='balanced_sparse':	
		# balanced
		mu = 1.0 
		o = scipy.sparse.csr_matrix((nf,nh))
		sys=wlmcmc.AB_SFC_macro1(nf,nh,o,o)
		F,g=sys.get_linear_system()	
		# sample consumption matrix 
		rvs = scipy.stats.lognorm(mu).rvs		
		A = scipy.sparse.random(nf*nh,1,density,data_rvs=rvs).tocsr()
		gg=F.tocsr()[:,0:nf*nh].dot(A)
		nnls = nls_solvers.LbfgsNNLS_L1(alpha = density)
		nnls.fit(F.tocsr()[:,nf*nh:2*nf*nh],g-gg.toarray().flatten()) 
		C = nnls.coef_.reshape((nf,nh))
		# csr reshape not implemented !!!!!
		A = A.toarray().reshape((nf,nh))
		
		return scipy.sparse.csr_matrix(A),scipy.sparse.csr_matrix(C)
	elif dataset=='balanced_ficm':	
		# balanced+ficm
		# sample topology from ficm
		# solve with nnls for some total weight
		pass

########################
# ISING
def get_high_energy_ising(N,M):
	"""
	return a high-energy ising field
	"""
	i=0;j=0
	field = np.ones((N,M),dtype=int)
	for k in range(N*M):
		(i,j)=np.unravel_index(k, (N,M)) 
		#if k%2==0: field[i,j] *=-1
		#print i,j,k%2
		field[i,j] = (-1)**(i+j)
	return field	

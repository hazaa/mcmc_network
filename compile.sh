#!/usr/bin/env bash
cython wl_mcmc_network.pyx -a
gcc -O3 -march=native wl_mcmc_network.c -shared -fPIC `python-config --cflags --libs` -o wl_mcmc_network.so

